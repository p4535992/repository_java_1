# README #

GraphHelper was developed as part of a Master's project undertaken by Heidi Olivia Munksgaard and Dines Madsen in 2013-2014. The application allows for the visualization of local and remote triple stores, as well as doing basic filtering actions in order to make complex graphs more readable.

In addition to the visualization elements, the application also contains two novel algorithms:
The first algorithm, Interesting Graph, searches in a local neighborhood of a node and scoring each node based on search-terms provided by the user. The exploration process is based on an A*-like algorithm, and the algorithm returns a valid RDF-sub graph for visualization.

The second algorithm searches for paths between two nodes. Each path is assigned a relevance score based on user entered search terms. The most relevant paths are returned and visualized.
In order to efficiently extract sub-graphs from remote triple stores, a custom breadth-first search algorithm was developed. Bucket Based Breadth First Search (BBBFS) searches outwards from a node iteratively, by first exploring the local neighborhood and then exploring the frontier in clusters. This method of reducing the number of calls to the triple store massively reduced the runtime cost of extract large sub-graphs.

### What is this repository for? ###

This repository is aimed towards computational intelligence researchers working with RDF graphs and intelligent searches.

### How do I get set up? ###

The repository is configured as a Netbeans project, and it is therefore easiest to setup using NetBeans. The application uses the following 3rd party libraries as NetBeans defined libraries:

* Apache Common IO - 2.4
* Apache Commons Collections - 4.0
* Google Guave - 17.0
* Jena - 2.11.0
* Jena Virtuoso Provider - 2
* JGraphX - 2.3.0.4
* Stanford NLP - CoreNLP Full 2014-01-04
* StringTemplate- 4.0.8
* Trove - 3.0.3

In addition an in-depth description of the code can be found in Report.pdf in the repository root.

### Who do I talk to? ###

In case of questions, please do not hesitate to contact:
M.Sc Dines Madsen - thedrows@gmail.com
M.Sc Heidi Olivia Munksgaard -  heidi.olivi@gmail.com