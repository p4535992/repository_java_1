/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.InterestingPaths;

import Graph.Enumerator.InterestingResult;
import java.util.ArrayList;

public class DisambiguityResults {

    public InterestingResult actionReport;
    public ArrayList<DisambiguityResult> myResults = new ArrayList<>();

    public DisambiguityResults(InterestingResult _report, ArrayList<DisambiguityResult> _results) {
        actionReport = _report;
        myResults = _results;
    }
}
