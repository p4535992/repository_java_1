/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.InterestingPaths;

import java.util.ArrayList;
import java.util.Objects;

public class InterestingPath implements Comparable<InterestingPath> {

    public ArrayList<String> myPath = new ArrayList<>();
    public double myScore = 0.0;

    /**
     * Determines the number of steps from start to finish of path
     *
     * @return Returns the number of steps
     */
    @SuppressWarnings("empty-statement")
    public int Steps() {
        int i;
        int steps = 0;
        for (i = 2; i < myPath.size(); i = i + 2) {
            steps++;
        }
        return steps;
    }

    /**
     * Returns the number of nodes between start and end node.
     *
     * @return Returns the number of intermediate nodes
     */
    public int IntermediateNodes() {
        int i;
        int intNodes = 0;
        for (i = 2; i < myPath.size() - 1; i = i + 2) {
            intNodes++;
        }
        return intNodes;
    }

    @Override
    public int compareTo(InterestingPath o) {
        if (myScore == o.myScore) {
            return 0;
        } else if (myScore > o.myScore) {
            return -1;
        } else {
            return 1;
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(this.myPath);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final InterestingPath other = (InterestingPath) obj;
        if (!Objects.equals(this.myPath, other.myPath)) {
            return false;
        }
        if (other.myPath.size() != myPath.size()) {
            return false;
        }
        for (int i = 0; i < myPath.size(); i++) {
            // Direct comparison of paths
            if (other.myPath.get(i) == null ? myPath.get(i) != null : !other.myPath.get(i).equals(myPath.get(i))) {
                return false;
            }
            // Reverse comparison (isomorphic)
            int invI = other.myPath.size() - 1 - i;
            if (Objects.equals(other.myPath.get(invI), myPath.get(i))) {
                return false;
            }
        }
        return true;
    }
}
