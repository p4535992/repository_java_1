/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.Algorithm.PBFS;

import Graph.Implementations.SPARQLFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.RDFNode;

import java.util.HashMap;
import java.util.Map;

public class MetaWorkUnit extends WorkUnit {

    Map<String, Integer> myResults = new HashMap<>();
    
    public MetaWorkUnit(String[] _filters) {
        myMaxLength = 4000;
        myBaselength = 257;
        
        // Calculating filtercosts
        myFilters = _filters;
        for (String s : myFilters) {
            myFilterCost += 4 * (33 + s.length());
        }
    }

    /**
     * Constructs a query string based on myURIs and myFilters
     *
     * @return SPARQL query string
     */
    public String getQuery() {
        return SPARQLFactory.GetCount(myURIs, myFilters);
    }

    /**
     * Parses a result set and adds the result to myResults
     *
     * @param _rs Result set to be parsed
     */
    public void parseResultSet(ResultSet _rs) {
        while (_rs.hasNext()) {
            QuerySolution qs = _rs.next();

            RDFNode n = qs.get("known");
            RDFNode nCount = qs.get("count");
            myResults.put(n.asResource().getURI(), nCount.asLiteral().getInt());
        }
    }

    /**
     * Returns the URIs and associated result set size
     *
     * @return HashMap of URIs and their associated result set size
     */
    public Map<String, Integer> getMyResults() {
        return myResults;
    }
}
