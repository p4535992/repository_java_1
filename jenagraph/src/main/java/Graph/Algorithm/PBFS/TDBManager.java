/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.Algorithm.PBFS;

import Graph.Implementations.StopWatch;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class TDBManager {

    private static final String path = "TDBStore";
    private static final DirectoryFilter myDirFilter = new DirectoryFilter();

    /**
     * Returns a model from TDBStore given the source, endpoint and filters. If
     * no model is found null is returned.
     *
     * @param _source Source node URI
     * @param _endpoint Endpoint URI
     * @param _filters Filter URIs
     * @return Returns a configured TDB or null if none is found in TDBStore
     */
    public static TDB getModel(String _source, String _endpoint, String[] _filters) {

        TDB result = null;
        File bDir = new File(path);
        if (bDir.exists()) {
            for (File dir : bDir.listFiles(myDirFilter)) {
                if (ValidateSettings(_source, _endpoint, _filters, dir)) {
                    result = new TDB(dir);
                }
            }
        } else {
            try {
                boolean b = bDir.mkdir();
            } catch (Exception ignored) {
            }
        }
        return result;
    }

    /**
     * Checks whether the TDBStore directory contains a model with the specified
     * source- and endpoint- URI, and with a subset of the provided filters
     *
     * @param _source Source node URI that the model was generated from
     * @param _endpoint URI for the endpoint
     * @param _filters Filter URIs of which the model must be a subset
     * @return True iff the conditions are met
     */
    public static boolean containsModel(String _source, String _endpoint, String[] _filters) {
        File bDir = new File(path);
        if (bDir.exists()) {
            for (File dir : bDir.listFiles(myDirFilter)) {
                if (ValidateSettings(_source, _endpoint, _filters, dir)) {
                    return true;
                }
            }
        } else {
            try {
                boolean b = bDir.mkdir();
            } catch (Exception ignored) {
            }
        }
        return false;
    }

    /**
     * Takes a File and compares it to the source, endpoint and filter URIs.
     * Source and endpoint must match and File's filters must be a subset of
     * filters.
     *
     * @param _source Source node URI
     * @param _endpoint Endpoint URI
     * @param _filters Filter URIs
     * @param _dir File object where settings.xml can be found.
     * @return True iff source/endpoint matches and settings.xml's filters is a
     * subset of _filters.
     */
    private static boolean ValidateSettings(String _source, String _endpoint, String[] _filters, File _dir) {

        try {
            File xmlDoc = new File(_dir, "settings.xml");
            if (xmlDoc.exists()) {
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(xmlDoc);

                doc.getDocumentElement().normalize();
                NodeList nList = doc.getElementsByTagName("settings");
                Node node = nList.item(0);

                Element root = doc.getDocumentElement();
                String source = root.getElementsByTagName("source").item(0).getTextContent();
                String endpoint = root.getElementsByTagName("endpoint").item(0).getTextContent();
                List<String> filters = new ArrayList<>();
                nList = doc.getElementsByTagName("filters");

                for (int i = 0; i < nList.getLength(); i++) {
                    Node nNode = nList.item(i);

                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        filters.add(((Element) nNode).getElementsByTagName("filter").item(0).getTextContent());
                    }
                }

                if (source == null ? _source != null : !source.equals(_source)) {
                    return false;
                }
                if (endpoint == null ? _endpoint != null : !endpoint.equals(_endpoint)) {
                    return false;
                }
                try {
                    for (String s : filters) {
                        List<String> temp = Arrays.asList(_filters);
                        if (!temp.contains(s)) {
                            return false;
                        }
                    }
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }


                return true;
            } else {
                return false;
            }
        } catch (ParserConfigurationException | SAXException | IOException e) {
            System.out.println(e.toString());
            return false;
        }
    }

    /**
     * Creates a TDB based on the provided information
     *
     * @param _source Source node URI
     * @param _endpoint Endpoint URI
     * @param _length Max distance from _source
     * @param _filters Filters used to create TDB
     * @return Returns newly created TDB, or if store already contains a model
     * with the values provided, that is returned
     */
    public static TDB CreateTDB(
            String _source,
            String _endpoint,
            int _length,
            String[] _filters) {

        if (containsModel(_source, _endpoint, _filters)) {
            return getModel(_source, _endpoint, _filters);
        } else {
            TDB tdb = new TDB(_source, 0, _filters, _endpoint);
            Integer hash = tdb.hashCode();
            File tdbPath = new File(path, hash.toString());
            boolean mkdir = tdbPath.mkdir();
            if(mkdir) {
                tdb.setMyPath(tdbPath);
                tdb.SaveSettings();
                FillTDB(_length, tdb);
                return tdb;
            }else{
                return null;
            }
        }
    }

    /**
     * Creates a TDB based on the provided information
     *
     * @param _source Source node URI
     * @param _endpoint Endpoint URI
     * @param _length Max distance from _source
     * @param _filters Filters used to create TDB
     * @return Returns newly created TDB, or if store already contains a model
     * with the values provided, that is returned
     */
    public static TDB CreateTDB1 (
            String _source,
            String _endpoint,
            int _length,
            String[] _filters) {

        if (containsModel(_source, _endpoint, _filters)) {
            return getModel(_source, _endpoint, _filters);
        } else {
            TDB tdb = new TDB(_source, 0, _filters, _endpoint);
            Integer hash = tdb.hashCode();
            File tdbPath = new File(path, hash.toString());
            boolean mkdir = tdbPath.mkdir();

            if(mkdir){
                tdb.setMyPath(tdbPath);
                tdb.SaveSettings();
                FillTDB1(_length, tdb);
                return tdb;
            }else{
                return null;
            }

        }
    }
    private static void FillTDB(int _distance, TDB _tdb) {
        InformationReporter iReport = new InformationReporter(1);
        StopWatch sw = new StopWatch();
        DataManager DM = new DataManager(_tdb.getMySource(), _tdb.getMyFilters(), iReport);
        
        int threads = Runtime.getRuntime().availableProcessors();
        MetaWorker[] metaWorkers = new MetaWorker[threads];
        DataWorker[] dataWorkers = new DataWorker[threads];
        _tdb.Connect();
        long start = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        for (int i = 1; i <= _distance; i++) {
            _tdb.IncrementLength();
            
            sw.start();
            for (int j = 0; j < metaWorkers.length; j++) {
                metaWorkers[j] = new MetaWorker(_tdb.getMyEndpoint(), DM);
                metaWorkers[j].start();
            }

            for (MetaWorker mw : metaWorkers) {
                try {
                    mw.join();
                } catch (InterruptedException ex) {
                    System.out.println(ex.toString());
                }
            }
            sw.stop();
            iReport.current.metaPhase = sw.getElapsedTime();
            sw.start();
            for (int j = 0; j < dataWorkers.length; j++) {
                dataWorkers[j] = new DataWorker(_tdb.getMyEndpoint(), DM, _tdb);
                dataWorkers[j].start();
            }

            for (DataWorker dw : dataWorkers) {
                try {
                    dw.join();
                } catch (Exception ex) {
                    System.out.println(ex.toString());
                }
            }
            sw.stop();
            iReport.current.dataPhase = sw.getElapsedTime();
            
            System.out.println("Memory Usage: " + ((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory())-start)/1024/1024);
            sw.start();
            DM.MoveFrontier();
            sw.stop();
            iReport.current.movePhase = sw.getElapsedTime();
            iReport.Next();
        }
        _tdb.WriteFrontier(DM.getFrontier());
        _tdb.Close();
        DM.Cleanup();
        iReport.Print();
    }

    public static void ExpandTDB(int _steps, TDB _tdb) {

        List<String> frontier = _tdb.ReadFrontier();
        DataManager DM;
        if (frontier.isEmpty()) {
            DM = new DataManager(_tdb.getMySource(), _tdb.getMyFilters());
        } else {
            DM = new DataManager(frontier, _tdb.getMyFilters());
        }

        _tdb.Connect();

        int threads = Runtime.getRuntime().availableProcessors();
        int distance = _tdb.getMyLength();
        MetaWorker[] metaWorkers = new MetaWorker[threads];
        DataWorker[] dataWorkers = new DataWorker[threads];

        for (int i = distance; i <= (distance + _steps); i++) {
            _tdb.IncrementLength();

            for (int j = 0; j < metaWorkers.length; j++) {
                metaWorkers[j] = new MetaWorker(_tdb.getMyEndpoint(), DM);
                metaWorkers[j].start();
            }

            for (MetaWorker mw : metaWorkers) {
                try {
                    mw.join();
                } catch (InterruptedException ex) {
                    System.out.println(ex.toString());
                }
            }

            for (int j = 0; j < dataWorkers.length; j++) {
                dataWorkers[j] = new DataWorker(_tdb.getMyEndpoint(), DM, _tdb);
                dataWorkers[j].start();
            }

            for (DataWorker dw : dataWorkers) {
                try {
                    dw.join();
                } catch (Exception ignored) {

                }
            }

            DM.MoveFrontier();
        }
        _tdb.WriteFrontier(DM.getFrontier());
        _tdb.Close();
    }
    
    private static void FillTDB1(int _distance, TDB _tdb) {
        InformationReporter iReport = new InformationReporter(1);
        StopWatch sw = new StopWatch();
        DataManager1 DM = new DataManager1(_tdb.getMySource(), _tdb.getMyFilters(), iReport);
        
        int threads = 4;// Runtime.getRuntime().availableProcessors();
        MetaWorker1[] metaWorkers = new MetaWorker1[threads];
        DataWorker1[] dataWorkers = new DataWorker1[threads];
        _tdb.Connect();
        
        long start = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        
        for (int i = 1; i <= _distance; i++) {
            _tdb.IncrementLength();
            
            sw.start();
            for (int j = 0; j < metaWorkers.length; j++) {
                metaWorkers[j] = new MetaWorker1(_tdb.getMyEndpoint(), DM);
                metaWorkers[j].start();
            }

            for (MetaWorker1 mw : metaWorkers) {
                try {
                    mw.join();
                } catch (InterruptedException ex) {
                    System.out.println(ex.toString());
                }
            }
            sw.stop();
            iReport.current.metaPhase = sw.getElapsedTime();
            sw.start();
            
            DM.JuggleBins();
            
            for (int j = 0; j < dataWorkers.length; j++) {
                dataWorkers[j] = new DataWorker1(_tdb.getMyEndpoint(), DM, _tdb);
                dataWorkers[j].start();
            }

            for (DataWorker1 dw : dataWorkers) {
                try {
                    dw.join();
                } catch (Exception ex) {
                    System.out.println(ex.toString());
                }
            }
            sw.stop();
            iReport.current.dataPhase = sw.getElapsedTime();
            
            _tdb.FlushWriteBuffer();
            System.out.println("Memory Usage: " + ((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory())-start)/1024/1024);
            sw.start();
            DM.MoveFrontier();
            sw.stop();
            iReport.current.movePhase = sw.getElapsedTime();
            iReport.Next();
        }
        _tdb.WriteFrontier(DM.getFrontier());
        _tdb.Close();
        DM.Cleanup();
        iReport.Print();
    }
    
    public static void main(String args[]) {
        String endpoint = "http://dbpedia.org/sparql";
        String[] filters = {"http://www.w3.org/2002/07/owl#", "http://www.w3.org/2000/01/rdf-schema#", "http://www.w3.org/1999/02/22-rdf-syntax-ns#", "http://dbpedia.org/class/yago/"};
        String seed = "http://dbpedia.org/resource/Alan_Turing";

        TDB tdb = CreateTDB1(seed, endpoint, 3, filters);
    }
}

/**
 * Only returns directories
 */
class DirectoryFilter implements FileFilter {

    @Override
    public boolean accept(File pathname) {
        return pathname.isDirectory();
    }
}
