/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.Algorithm.PBFS;

import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;

public class Bin {

    private final TIntList myNodes = new TIntArrayList();
    private int myVolume = 0;
    
    public int getVolume() {
        return myVolume;
    }

    public void add2Bin(int _member, int _volume) {
        myNodes.add(_member);
        myVolume += _volume;
    }
    
    public int getCount() {
        return myNodes.size();
    }
    
    public TIntList getMembers() {
        return myNodes;
    }
    
}
