/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.Algorithm.PBFS;

import java.util.ArrayList;
import java.util.List;

public class IterationData {

    public IterationData(int _itr) {
        Iteration = _itr;
    }
    
    public int Iteration;
    public int metaUnitCount = 0;
    public List<Integer> metaUnitSizes = new ArrayList<>();
    public int dataUnitCount = 0;
    public List<Integer> dataUnitSizes = new ArrayList<>();
    public long metaPhase = 0;
    public long dataPhase = 0;
    public long movePhase = 0;
    public double optimalNumOfPackages = 0;
    
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        result.append("Iteration: ").append(Iteration);
        result.append("\nMetaPackages: ").append(metaUnitCount);
        result.append("\nOptimal Packaging: ").append(optimalNumOfPackages);
        result.append("\nDataPackages: ").append(dataUnitCount);
        result.append("\nMeta Phase(ms): ").append(metaPhase);
        result.append("\nData Phase(ms): ").append(dataPhase);
        result.append("\nMove Phase(ms): ").append(movePhase);
        result.append("\n------------ MetaUnit sizes ------------");
        for (Integer i : metaUnitSizes) {
            result.append("\n").append(i);
        }
        result.append("\n------------ DataUnit sizes ------------");
        for (Integer i : dataUnitSizes) {
            result.append("\n").append(i);
        }
        return result.toString();
    }
}
