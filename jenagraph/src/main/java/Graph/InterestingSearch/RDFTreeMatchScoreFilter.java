/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.InterestingSearch;

import java.util.List;

public class RDFTreeMatchScoreFilter {
    private RDFTree myTree = null;

    /**
     * Used to filter out nodes without any MatchScore.
     * @param _tree Tree to be filtered.
     */
    public RDFTreeMatchScoreFilter(RDFTree _tree) {
        myTree = _tree;
    }

    /**
     * Filters the original tree based on positive match scores Nodes that have
     * a positive match score are kept, as are nodes necessary in order to reach
     * positive match nodes.
     */
    public void filterOriginal() {
        containsMatchedNodes(myTree.getRoot());
    }

    /**
     * Filters a copy of the original tree based on positive match scores Nodes
     * that have a positive match score are kept, as are nodes necessary in
     * order to reach positive match nodes.
     *
     * @return Filtered copy of original tree
     */
    public RDFTree filterCopy() {
        RDFTree temp = myTree.copy();
        containsMatchedNodes(temp.getRoot());
        return temp;
    }

    /**
     * Filters a copy of the original tree based on positive match scores Nodes
     * that have a positive match score are kept, as are nodes necessary in
     * order to reach positive match nodes.
     *
     * @param _tree Location where filtered tree is to be stored.
     */
    public void filterCopy(RDFTree _tree) {
        _tree = myTree.copy();
        containsMatchedNodes(_tree.getRoot());
    }

    private boolean containsMatchedNodes(RDFTreeNode _node) {
        if (_node.getMyChildren() == null) {
            return (_node.getMyMatchScore() > 0);
        } else if (_node.getMyChildren().isEmpty()) {
            return (_node.getMyMatchScore() > 0);
        } // Node has children, check each child
        // and remove the ones without match score
        else {
            List<RDFTreeNode> nodeChildren = _node.getMyChildren();
            for (int i = nodeChildren.size() - 1; i >= 0; i--) {
                if (containsMatchedNodes(nodeChildren.get(i)) == false) {
                    nodeChildren.remove(i);
                }
            }
            // We don't have any children left, so return own score
            if (nodeChildren.isEmpty()) {
                return (_node.getMyMatchScore() > 0);
            } // We do have children with scores left, so return true
            else {
                return true;
            }
        }
    }
}
