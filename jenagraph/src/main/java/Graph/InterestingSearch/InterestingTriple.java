/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.InterestingSearch;

import Graph.Enumerator.NodeType;
import Graph.Enumerator.PredicateDirection;

public class InterestingTriple {
    public String nodeOne;
    public String nodeOneShort;
    public NodeType nodeTypeOne;
    public double nodeValueOne;
    
    public String nodeTwo;
    public String nodeTwoShort;
    public NodeType nodeTypeTwo;
    public double nodeValueTwo;
    
    public PredicateDirection predDir;
    public String[] predicateValues;
    public String[] predicateShorts;
    
    public InterestingTriple(
            String _nodeOne,
            String _nodeOneShort,
            NodeType _nodeTypeOne,
            double _nodeValueOne,
            String _nodeTwo,
            String _nodeTwoShort,
            NodeType _nodeTypeTwo,
            double _nodeValueTwo,
            PredicateDirection _predDir,
            String[] _predicateValues,
            String[] _predicateShorts) {
        
        nodeOne = _nodeOne;
        nodeOneShort = _nodeOneShort;
        nodeTypeOne = _nodeTypeOne;
        nodeValueOne = _nodeValueOne;
        
        nodeTwo = _nodeTwo;
        nodeTwoShort = _nodeTwoShort;
        nodeTypeTwo = _nodeTypeTwo;
        nodeValueTwo = _nodeValueTwo;
        
        predDir = _predDir;
        predicateValues = _predicateValues;
        predicateShorts = _predicateShorts;
    }
    
    public InterestingTriple()
    {
        
    }
}
