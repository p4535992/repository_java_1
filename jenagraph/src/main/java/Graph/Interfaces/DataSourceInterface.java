/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.Interfaces;

import GUI.Threads.InterestingGraphThread;
import GUI.Threads.InterestingPathsThread;
import Graph.Enumerator.DeleteEdge;
import Graph.Enumerator.DeleteLiteral;
import Graph.Enumerator.InsertLiteral;
import Graph.Enumerator.UpdateLiteralText;
import Graph.Enumerator.NodeType;
import Graph.Enumerator.ReadyToVisualise;
import Graph.Enumerator.DeleteVertex;
import Graph.InterestingSearch.InterestingLiterals;
import Graph.Implementations.ResourceResult;
import Graph.Implementations.SPARQL_ResultSet;
import Graph.Implementations.TripleStatement;
import Graph.Implementations.TripleStatementWrapper;
import Graph.Implementations.VisualiseWrapper;
import Graph.InterestingPaths.DisambiguityResults;
import Graph.InterestingPaths.InterestingPath;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.sparql.engine.http.QueryExceptionHTTP;

import java.util.ArrayList;

public interface DataSourceInterface {
    SPARQL_ResultSet performSelect(String _query);
    Model performConstruct(String _query) throws QueryExceptionHTTP;
    boolean performAsk(String _query) throws QueryExceptionHTTP;
    boolean verifyConnection();
    ReadyToVisualise ReadyToVisualise();
    ArrayList<TripleStatement> GetStatements();
    void UpdateModel() throws QueryExceptionHTTP;
    void UpdateModel(String _newCenter, NodeType _type) throws QueryExceptionHTTP;
    void setCenterVertex(String _newCenter, NodeType _type);
    String getCenterVertex();
    NodeType getNodeType(String _value);
    TripleStatementWrapper InterestingSearch(
            String _bagOfWords, int _maxNodes, int _maxDist,
            String _initNode, NodeType _initType, double _beta,
            String[] _filters, InterestingGraphThread _report,
            String _lang);
    InterestingPath[] InterestingPaths(String[] _NSFilters,
                                              String _source, String _target,
                                              String _searchTerms, String _lang,
                                              int _minPath, int _maxPath, int _maxNumOfPaths, InterestingPathsThread _report);
    
    DisambiguityResults DisambiguitySearch(String _searchTerm, String _lang);
    
    int getMaxVertices();
    void setMaxVertices(int _max);
    VisualiseWrapper CenterOn(String _center, NodeType _type);
    VisualiseWrapper CenterOn();
    ArrayList<InterestingPath> FetchPaths(String _query) throws Exception;
    void setTimeout(int _timeout);
    int getTimeout();
    String getEndpointURL();
    void setEndpointURL(String _newURI);
    
    // Vertex commands
    ResourceResult UpdateVertexURI(String _vertex,
                                          String _newURI);
    ResourceResult VertexToBlankNode(String _vertex);
    DeleteVertex DeleteVertex(String _node);
    ResourceResult InsertVertex(String _value);
    ArrayList<TripleStatement> CBD(String _vertex);
    
    // Blank Node commands
    ResourceResult ResolveBlankNode(String _nodeID,
                                           String _newURI);
    DeleteVertex DeleteBlankNode(String _nodeID);
    ResourceResult InsertBlankNode();
    
    // Literal commands
    UpdateLiteralText UpdateLiteralText(String _literal,
                                               String _newText);
    DeleteLiteral DeleteLiteral(String _literal);
    InsertLiteral InsertLiteral(String _text);
    ArrayList<TripleStatement> SCBD(String _literal);
    InterestingLiterals getLiterals(String _center, NodeType _centerType);
    
    // Edge commands
    DeleteEdge DeleteEdge(String _model,
                                 String _edgeURI, String _source,
                                 NodeType _sourceType, String _target,
                                 NodeType _targetType);
   ResourceResult ChangeEdgeURI(String _edgeURI,
                                        String _source, NodeType _sourceType,
                                        String _target, NodeType _targetType,
                                        String _newEdgeURI);
    ResourceResult InsertEdge(String _edgeURI,
                                     String _source, NodeType _sourceType,
                                     String _target, NodeType _targetType);
}
