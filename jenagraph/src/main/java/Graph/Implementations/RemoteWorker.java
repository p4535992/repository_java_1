/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.Implementations;

import org.apache.jena.query.*;
import org.apache.jena.rdf.model.*;

import java.util.ArrayList;
import java.util.List;

class RemoteWorker extends Thread {

    private final List<String> myQueries;
    private final String myURI;
    private final DataContainer myModel;
    private final boolean myInboundSearch;

    public RemoteWorker(List<String> _queries, DataContainer _m, String _URI, boolean _inbound) {
        myQueries = _queries;
        myURI = _URI;
        myModel = _m;
        myInboundSearch = _inbound;
    }

    @Override
    public void run() {
        List<Statement> list = new ArrayList<>(10000);

        for (String q : myQueries) {
            ResultSet rs = Query(q);
            ProcessResults(rs, list);
        }

        // Adding processed values
        if (myInboundSearch) {
            myModel.InsertStatement(list, true);
        } else {
            myModel.InsertStatement(list, false);
        }
    }

    private ResultSet Query(String _q) {
        try {
            Query query = QueryFactory.create(_q);
            QueryExecution qExe = QueryExecutionFactory.sparqlService(myURI, query);
            return qExe.execSelect();
        } catch (Exception e) {
            System.out.println("------------------------");
            System.out.println(e.getMessage());
            return null;
        }
    }

    private void ProcessResults(ResultSet rs, List<Statement> _list) {
        if (rs != null) {
            while (rs.hasNext()) {
                QuerySolution sol = rs.next();
                Resource sbj = sol.getResource("s");
                RDFNode obj = sol.get("o");
                Property pred = ResourceFactory.createProperty(sol.get("p").toString());

                Statement s = ResourceFactory.createStatement(sbj, pred, obj);
                if (!_list.contains(s)) {
                    _list.add(s);
                }
            }
        }

    }
}
