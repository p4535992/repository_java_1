/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.Implementations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.StmtIterator;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupFile;

public class SPARQLFactory {

    private static final String path = "templates";

    /**
     * Takes an array of URL's and generates one or more SPARQL expression that
     * will extract all relevant literals. Each expression is max 2000
     * characters long.
     *
     * @param _sbjs The URI's to be extract literals based on
     * @param _lang An optional language. If set to null, language will not be
     * used.
     * @return SPPARQL queries as a String[]
     */
    public static String[] GetMultipleLiterals(String[] _sbjs, String _lang) {

        String[] result;
        ArrayList<int[]> myIntervals = new ArrayList<>();
        int start = 0, length = 102, max = 2000, i, j = 0;

        System.out.println("");
        for (i = 0; i < _sbjs.length; i++) {
            if (length + _sbjs[i].length() < max) {
                length += _sbjs[i].length();
            } else {
                int[] myInts = new int[2];
                myInts[0] = start;
                myInts[1] = i - 1;
                myIntervals.add(myInts);
                length = _sbjs[i].length() + 102;
                start = i;
            }
        }
        if (start <= i) {
            int[] myInts = new int[2];
            myInts[0] = start;
            myInts[1] = i;
            myIntervals.add(myInts);
        }

        result = new String[myIntervals.size()];
        for (int[] v : myIntervals) {
            String[] temp = Arrays.copyOfRange(_sbjs, v[0], v[1]);

            result[j] = GetLiterals(temp, _lang);
            j++;
        }
        return result;
    }

    /**
     * Takes an array of URL's and generates a SPARQL expression that will
     * extract all relevant literals.
     *
     * @param _sbjs The URI's to be extract literals based on
     * @param _lang An optional language. If set to null, language will not be
     * used.
     * @return SPPARQL query as a String
     */
    public static String GetLiterals(String[] _sbjs, String _lang) {
        STGroup myGroup = new STGroupFile(path + "\\Literals.stg");
        ST result = myGroup.getInstanceOf("GET_LITERALS");

        if (_sbjs.length == 1) {
            result.add("sbjs", "<" + _sbjs[0] + ">");
        } else {
            result.add("sbjs", "<" + _sbjs[0] + ">");
            for (int i = 1; i < _sbjs.length; i++) {
                result.add("sbjs", ", <" + _sbjs[i] + ">");
            }
        }

        // Add language, if null template will ignore
        result.add("lang", _lang);
        return result.render();
    }

    /**
     * Generates an array of SPARQL queries for paths from start to end with a
     * number of intermediate nodes of k
     *
     * @param _k Number of intermediate nodes
     * @param _start URL for initial node
     * @param _end URL for end node
     * @param _filters list of predicate types not to be followed
     * @return Array of SPARQL queries
     */
    public static String[] GenerateQueries(int _k, String _start, String _end, String[] _filters) {

        if (_k == 1) {
            String[] result = new String[4];
            result[0] = DirectConnection(1, _start, _end, _filters);
            result[1] = DirectConnection(1, _end, _start, _filters);
            result[2] = TowardsCommonNodeConnection(1, 1, _start, _end, _filters);
            result[3] = AwayFromCommonNodeConnection(1, 1, _start, _end, _filters);
            return result;
        } else {
            ArrayList<String> result = new ArrayList<>();
            result.add(DirectConnection(_k, _start, _start, _filters));
            result.add(DirectConnection(_k, _end, _start, _filters));

            for (int i = 1; i <= _k; i++) {
                result.add(AwayFromCommonNodeConnection(_k, i, _start, _end, _filters));
            }

            for (int i = 1; i <= _k; i++) {
                result.add(TowardsCommonNodeConnection(_k, i, _start, _end, _filters));
            }
            return result.toArray(new String[result.size()]);
        }
    }

    /**
     * Creates SPARQL expression with k intermediate nodes, and all paths
     * pointing away from n.
     *
     * @param _k Number of intermediate nodes =< 1
     * @param _n Center node for intermediate nodes
     * @param _start URL starting point
     * @param _end URL ending point
     * @param _filters Predicates to not be followed
     * @return SPARQL query as String
     */
    public static String AwayFromCommonNodeConnection(int _k, int _n, String _start, String _end, String[] _filters) {
        STGroup myGroup = new STGroupFile(path + "\\AwayFromCommonNode.stg");

        if (_k == 1) {
            ST result = myGroup.getInstanceOf("AWAY_FROM_COMMON_NODE_K1");
            result.add("start", _start);
            result.add("end", _end);
            result.add("filters", _filters);
            return result.render();
        } else {
            ST result = myGroup.getInstanceOf("TOWARDS_COMMON_NODE");

            // Select segment
            ST select = myGroup.getInstanceOf("SELECT");
            select.add("start", _start);
            select.add("end", _end);

            int j;
            for (j = 1; j <= _k; j++) {
                select.add("itr", j);
            }

            select.add("itrPlus", (j));
            result.add("select", select);

            // Start segment
            ST start = myGroup.getInstanceOf("START");
            start.add("start", _start);
            result.add("start", start);

            // left-hand side of center node
            for (j = 1; j < _n; j++) {
                ST left = myGroup.getInstanceOf("LEFT");
                left.add("i", j);
                left.add("j", (j + 1));
                result.add("middle", left);
            }

            // right-hand side of center node
            for (j = _n; j <= _k - 1; j++) {
                ST right = myGroup.getInstanceOf("RIGHT");
                right.add("i", (j + 1));
                right.add("j", j);
                result.add("middle", right);
            }

            // End segment
            // END(end, i, j) ::= "?o<i> ?p<j> \<<end>\> ."
            ST end = myGroup.getInstanceOf("END");
            end.add("end", _end);
            end.add("i", j);
            end.add("j", (j + 1));
            result.add("end", end);

            // Adding filters
            ST filters = myGroup.getInstanceOf("FILTER");
            filters.add("nodes", getIntArray(_k));
            filters.add("predicates", getIntArray(_k + 1));
            filters.add("filters", _filters);

            //FILTER_LOOPBACK(start, end, iArray, j) ::=
            for (int z = 1; z <= _k; z++) {
                ST loopback = myGroup.getInstanceOf("FILTER_LOOPBACK");
                loopback.add("start", _start);
                loopback.add("end", _end);
                loopback.add("j", z);
                loopback.add("iArray", getIntArray(_k, z));
                filters.add("references", loopback);
            }
            result.add("filters", filters);

            return result.render();
        }
    }

    /**
     * Creates SPARQL expression with k intermediate nodes, and all paths
     * pointing towards n.
     *
     * @param _k Number of intermediate nodes =< 1
     * @param _n Center node for intermediate nodes
     * @param _start URL starting point
     * @param _end URL ending point
     * @param _filters Predicates to not be followed
     * @return SPARQL query as String
     */
    public static String TowardsCommonNodeConnection(int _k, int _n, String _start, String _end, String[] _filters) {
        STGroup myGroup = new STGroupFile(path + "\\TowardsCommonNode.stg");

        if (_k == 1) {
            ST result = myGroup.getInstanceOf("TOWARDS_COMMON_NODE_K1");
            result.add("start", _start);
            result.add("end", _end);
            result.add("filters", _filters);
            return result.render();
        } else {
            ST result = myGroup.getInstanceOf("TOWARDS_COMMON_NODE");

            // Select segment
            ST select = myGroup.getInstanceOf("SELECT");
            select.add("start", _start);
            select.add("end", _end);

            int j;
            for (j = 1; j <= _k; j++) {
                select.add("itr", j);
            }

            select.add("itrPlus", (j));
            result.add("select", select);

            // Start segment
            ST start = myGroup.getInstanceOf("START");
            start.add("start", _start);
            result.add("start", start);

            // left-hand side of center node
            for (j = 1; j < _n; j++) {
                ST left = myGroup.getInstanceOf("LEFT");
                left.add("i", j);
                left.add("j", (j + 1));
                result.add("middle", left);
            }

            // right-hand side of center node
            for (j = _n; j <= _k - 1; j++) {
                ST right = myGroup.getInstanceOf("RIGHT");
                right.add("i", (j + 1));
                right.add("j", j);
                result.add("middle", right);
            }

            // End segment
            ST end = myGroup.getInstanceOf("END");
            end.add("end", _end);
            end.add("i", j);
            end.add("j", (j + 1));
            result.add("end", end);

            // Adding filters
            // FILTER(nodes, predicates, filters) ::=
            ST filters = myGroup.getInstanceOf("FILTER");
            filters.add("nodes", getIntArray(_k));
            filters.add("predicates", getIntArray(_k + 1));
            filters.add("filters", _filters);

            //FILTER_LOOPBACK(start, end, iArray, j) ::=
            for (int z = 1; z <= _k; z++) {
                ST loopback = myGroup.getInstanceOf("FILTER_LOOPBACK");
                loopback.add("start", _start);
                loopback.add("end", _end);
                loopback.add("j", z);
                loopback.add("iArray", getIntArray(_k, z));
                filters.add("references", loopback);
            }

            result.add("filters", filters);
            return result.render();
        }
    }

    /**
     * Creates a SPARQL expression going from _start to _stop over _k steps
     *
     * @param _k Number of in-between nodes
     * @param _start Start node
     * @param _stop End node
     * @return SPARQL Expression as string
     */
    public static String DirectConnection(int _k, String _start, String _stop, String[] _filters) {
        STGroup myGroup = new STGroupFile(path + "\\Direct.stg");

        if (_k == 1) {
            ST result = myGroup.getInstanceOf("DIRECT_K1");
            result.add("start", _start);
            result.add("end", _stop);
            result.add("filters", _filters);
            return result.render();
        } else {
            ST result = myGroup.getInstanceOf("BODY");

            // Select segment
            ST select = myGroup.getInstanceOf("SELECT");
            select.add("start", _start);
            select.add("end", _stop);

            int j;
            for (j = 1; j <= _k; j++) {
                select.add("itr", j);
            }
            select.add("itrPlus", (j));
            result.add("select", select);

            // Start segment
            ST start = myGroup.getInstanceOf("START");
            start.add("start", _start);
            start.add("end", _stop);
            result.add("start", start);

            // Middle
            int i;
            for (i = 1; i < _k; i++) {
                ST middle = myGroup.getInstanceOf("MIDDLE");
                middle.add("i", i);
                middle.add("j", (i + 1));
                result.add("middle", middle);
            }

            // End
            ST end = myGroup.getInstanceOf("END");
            end.add("end", _stop);
            end.add("i", i);
            end.add("j", i + 1);
            result.add("end", end);

            // Adding filters
            ST filters = myGroup.getInstanceOf("FILTER");
            filters.add("nodes", getIntArray(_k));
            filters.add("predicates", getIntArray(_k + 1));
            filters.add("filters", _filters);

            //FILTER_LOOPBACK(start, end, iArray, j) ::=
            for (int z = 1; z <= _k; z++) {
                ST loopback = myGroup.getInstanceOf("FILTER_LOOPBACK");
                loopback.add("start", _start);
                loopback.add("end", _stop);
                loopback.add("j", z);
                loopback.add("iArray", getIntArray(_k));
                filters.add("references", loopback);
            }
            result.add("filters", filters);
            return result.render();
        }
    }

    /**
     * Returns a formatted string of ?o<i> from _s to _e, skipping _m
     *
     * @param _s Lower bound
     * @param _e Upper bound
     * @param _m left out
     * @return Formatted string
     */
    private static String GenerateIntegerArray(int _s, int _e, int _m) {

        StringBuilder result = new StringBuilder();
        for (int i = _s; i <= _e; i++) {
            if (i != _m) {
                if (i == _s && result.length() == 0) {
                    result.append("?o").append(i);
                } else if (i != _s && result.length() == 0) {
                    result.append("?o").append(i);
                } else {
                    result.append(", ?o").append(i);
                }
            }
        }
        return result.toString();
    }

    /**
     * Returns an array from one to k with _m left out.
     *
     * @param k Upper bound
     * @param _m Integer left out
     * @return Array of size k-1
     */
    private static int[] getIntArray(int k, int _m) {
        int[] result = new int[k - 1];
        int ptr = 0;
        for (int i = 1; i <= k; i++) {
            if (i != _m) {
                result[ptr] = i;
                ptr++;
            }
        }
        return result;
    }

    /**
     * returns an array of integers from 1 to i
     *
     * @param i Upperbound
     * @return Array of integers from 1 to i
     */
    private static int[] getIntArray(int i) {
        int[] result = new int[i];
        for (int j = 1; j <= i; j++) {
            result[j - 1] = j;
        }
        return result;
    }

    public static Model ExpandModelInwards(Set<String> _frontier, String[] _filters) {
        STGroup myGroup = new STGroupFile(path + "\\Trial.stg");
        String[] frontierArray = new String[_frontier.size()];
        _frontier.toArray(frontierArray);

        ST q = myGroup.getInstanceOf("EXPAND_MODEL_INWARDS");
        q.add("URIs", frontierArray);
        q.add("filters", _filters);

        System.out.println(q.render());
        //Model m = ModelFactory.createDefaultModel();
        Query myQuery = QueryFactory.create(q.render());
        QueryExecution qExe = QueryExecutionFactory.sparqlService("http://dbpedia.org/sparql", myQuery);
        return qExe.execConstruct();
    }

    public static String[] MultipleExpandInwards(List<String> _frontier, String[] _filters) {
        STGroup myGroup = new STGroupFile(path + "\\Trial.stg");
        String[] result;
        ArrayList<int[]> myIntervals = new ArrayList<>();
        int start = 0, length = 238, max = 2000, i, j = 0;

        for (i = 0; i < _frontier.size(); i++) {
            if (length + _frontier.get(i).length() < max) {
                length += _frontier.get(i).length();
            } else {
                int[] myInts = new int[2];
                myInts[0] = start;
                myInts[1] = i - 1;
                myIntervals.add(myInts);
                length = _frontier.get(i).length() + 238;
                start = i;
            }
        }
        if (start <= i) {
            int[] myInts = new int[2];
            myInts[0] = start;
            myInts[1] = i;
            myIntervals.add(myInts);
        }

        result = new String[myIntervals.size()];
        for (int[] v : myIntervals) {
            String[] temp = new String[v[1] - v[0]];
            _frontier.subList(v[0], v[1]).toArray(temp);
            ST q = myGroup.getInstanceOf("EXPAND_MODEL_INWARDS");
            q.add("URIs", temp);
            q.add("filters", _filters);

            result[j] = q.render();
            j++;
        }
        return result;
    }
    
    public static String[] MultipleExpandInwards(String[] _frontier, String[] _filters) {
        STGroup myGroup = new STGroupFile(path + "\\Trial.stg");
        String[] result;
        ArrayList<int[]> myIntervals = new ArrayList<>();
        int start = 0, length = 238, max = 2000, i, j = 0;

        for (i = 0; i < _frontier.length; i++) {
            if (length + _frontier[i].length() < max) {
                length += _frontier[i].length();
            } else {
                int[] myInts = new int[2];
                myInts[0] = start;
                myInts[1] = i - 1;
                myIntervals.add(myInts);
                length = _frontier[i].length() + 238;
                start = i;
            }
        }
        if (start <= i) {
            int[] myInts = new int[2];
            myInts[0] = start;
            myInts[1] = i;
            myIntervals.add(myInts);
        }

        result = new String[myIntervals.size()];
        for (int[] v : myIntervals) {
            String[] temp = Arrays.copyOfRange(_frontier, v[0], v[1]);
            ST q = myGroup.getInstanceOf("EXPAND_MODEL_INWARDS");
            q.add("URIs", temp);
            q.add("filters", _filters);

            result[j] = q.render();
            j++;
        }
        return result;
    }

    
    public static String[] MultipleExpandOutwards(List<String> _frontier, String[] _filters) {
        STGroup myGroup = new STGroupFile(path + "\\Trial.stg");
        String[] result;
        ArrayList<int[]> myIntervals = new ArrayList<>();
        int start = 0, length = 238, max = 2000, i, j = 0;

        for (i = 0; i < _frontier.size(); i++) {
            if (length + _frontier.get(i).length() < max) {
                length += _frontier.get(i).length();
            } else {
                int[] myInts = new int[2];
                myInts[0] = start;
                myInts[1] = i - 1;
                myIntervals.add(myInts);
                length = _frontier.get(i).length() + 238;
                start = i;
            }
        }
        if (start <= i) {
            int[] myInts = new int[2];
            myInts[0] = start;
            myInts[1] = i;
            myIntervals.add(myInts);
        }

        result = new String[myIntervals.size()];
        for (int[] v : myIntervals) {
            String[] temp = new String[v[1] - v[0]];
            _frontier.subList(v[0], v[1]).toArray(temp);
            ST q = myGroup.getInstanceOf("EXPAND_MODEL_OUTWARDS");
            q.add("URIs", temp);
            q.add("filters", _filters);

            result[j] = q.render();
            j++;
        }
        return result;
    }
    
    public static String[] MultipleExpandOutwards(String[] _frontier, String[] _filters) {
        STGroup myGroup = new STGroupFile(path + "\\Trial.stg");
        String[] result;
        ArrayList<int[]> myIntervals = new ArrayList<>();
        int start = 0, length = 238, max = 2000, i, j = 0;

        for (i = 0; i < _frontier.length; i++) {
            if (length + _frontier[i].length() < max) {
                length += _frontier[i].length();
            } else {
                int[] myInts = new int[2];
                myInts[0] = start;
                myInts[1] = i - 1;
                myIntervals.add(myInts);
                length = _frontier[i].length() + 238;
                start = i;
            }
        }
        if (start <= i) {
            int[] myInts = new int[2];
            myInts[0] = start;
            myInts[1] = i;
            myIntervals.add(myInts);
        }

        result = new String[myIntervals.size()];
        for (int[] v : myIntervals) {
            String[] temp = Arrays.copyOfRange(_frontier, v[0], v[1]);
            ST q = myGroup.getInstanceOf("EXPAND_MODEL_OUTWARDS");
            q.add("URIs", temp);
            q.add("filters", _filters);

            result[j] = q.render();
            j++;
        }
        return result;
    }

    public static String GetNeighbourQuery(String _uri, String[] _filters) {
        STGroup myGroup = new STGroupFile(path + "\\Trial.stg");
        ST template = myGroup.getInstanceOf("INANDOUT");
        template.add("URI", _uri);
        template.add("filters", _filters);
        return template.render();
    }
    
    public static String GetInQuery(String _uri, String[] _filters, int _offset) {
        STGroup myGroup = new STGroupFile(path + "\\Trial.stg");
        ST template = myGroup.getInstanceOf("INBOUND");
        template.add("URI", _uri);
        template.add("filters", _filters);
        if (_offset > 0) {
            template.add("offset", _offset);
        }
        return template.render();
    }
    
    public static String GetOutQuery(String _uri, String[] _filters, int _offset) {
        STGroup myGroup = new STGroupFile(path + "\\Trial.stg");
        ST template = myGroup.getInstanceOf("OUTBOUND");
        template.add("URI", _uri);
        template.add("filters", _filters);
        if(_offset > 0) {
            template.add("offset", _offset);
        }
        return template.render();
    }
    
    public static Model ExpandModelOutwards(Set<String> _frontier, String[] _filters) {
        String[] frontierArray = new String[_frontier.size()];
        _frontier.toArray(frontierArray);

        String[] queries = MultipleExpandOutwards(frontierArray, _filters);
        Model m = ModelFactory.createDefaultModel();

        for (String s : queries) {
            Query myQuery = QueryFactory.create(s);
            QueryExecution qExe = QueryExecutionFactory.sparqlService("http://dbpedia.org/sparql", myQuery);
            Model temp = qExe.execConstruct();

            StmtIterator stmtItr = temp.listStatements();
            while (stmtItr.hasNext()) {
                m.add(stmtItr.next());
            }
            qExe.close();
        }
        return m;
    }
    
    public static String GetCount(List<String> _URIs, String[] _filters) {
        STGroup myGroup = new STGroupFile(path + "\\trial.stg");
        ST template = myGroup.getInstanceOf("COUNT_TRIPLES");
        
        template.add("URIs", _URIs);
        template.add("filters", _filters);
        
        return template.render();
    }
    
    public static String GetNightbours(List<String> _URIs, String[] _filters, int _offset) {
        STGroup myGroup = new STGroupFile(path + "\\Trial.stg");
        ST template = myGroup.getInstanceOf("BULKFETCH");
        
        template.add("URIs", _URIs);
        template.add("filters", _filters);
        if(_offset > 0) {
            template.add("offset", _offset);
        }
        return template.render();
    }
    public static List<List<String>> ChopArray(String[] _list, int _threds) {
        List<List<String>> parts = new ArrayList<>();
        if (_list.length < _threds) {
            for (String a_list : _list) {
                ArrayList<String> al = new ArrayList<>();
                al.add(a_list);
                parts.add(al);
            }
        } else {
            for (int i = 0; i < _threds; i++) {
                parts.add(new ArrayList<String>());
            }

            int ptr = 0;
            for (String s : _list) {
                parts.get(ptr).add(s);
                ptr++;
                if (ptr >= parts.size()) {
                    ptr = 0;
                }
            }
        }
        return parts;
    }

    /**
     * For debugging purposes only
     *
     * @param args the args.
     */
    public static void main(String[] args) {
//        String ein = "http://dbpedia.org/resource/Albert_Einstein";
//        String godel = "http://dbpedia.org/resource/Kurt_Godel";
//        String[] filters = {"http://www.w3.org/2002/07/owl#", "http://www.w3.org/2000/01/rdf-schema#", "http://www.w3.org/1999/02/22-rdf-syntax-ns#"};
//        //String URI = "http://dbpedia.org/sparql";
//        String URI = "http://thedrows.com:8890/sparql";
//
//        DataContainer myModel = new DataContainer(ein);
//        String[] queries1, queries2;
//        int threadCount = 8;
//        RemoteWorker[] threadArray = new RemoteWorker[threadCount];
//        StopWatch sw = new StopWatch();
//        for (int i = 1; i <= 2; i++) {
//            //String[] frontierNodes = new String[myFrontier.size()];
//            String[] frontierNodes = myModel.getFrontier();
//            queries1 = MultipleExpandOutwards(frontierNodes, filters);
//            queries2 = MultipleExpandInwards(frontierNodes, filters);
//            List<List<String>> secOut = ChopArray(queries1, threadCount);
//            List<List<String>> secIn = ChopArray(queries2, threadCount);
//            myModel.clearFrontier();
//            
//            // Fetching outbound triples
//            System.out.println("Outbound: " + i);
//            for (int j = 0; j < secOut.size(); j++) {
//                try {
//                    threadArray[j] = new RemoteWorker(secOut.get(j), myModel, URI, false);
//                    threadArray[j].start();
//                } catch (Exception e) {
//                }
//            }
//
//            // Waiting for threads to finish...
//            for (int j = 0; j < secOut.size(); j++) {
//                try {
//                    threadArray[j].join();
//                } catch (InterruptedException ex) {
//                    Logger.getLogger(SPARQLFactory.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//
//            System.out.println("Inbound: " + i);
//            // Fetching inbound triples
//            sw.start();
//            for (int j = 0; j < secIn.size(); j++) {
//                threadArray[j] = new RemoteWorker(secIn.get(j), myModel, URI, true);
//                threadArray[j].start();
//            }
//
//            // Waiting for threads to finish...
//            for (int j = 0; j < secIn.size(); j++) {
//                try {
//                    threadArray[j].join();
//                } catch (InterruptedException ex) {
//                    Logger.getLogger(SPARQLFactory.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//        myModel.close();
    }
}
