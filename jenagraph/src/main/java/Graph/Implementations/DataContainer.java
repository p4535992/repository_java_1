/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.Implementations;

import GUI.CustomControls.TabContainer.Result4Frontier;
import org.apache.jena.query.Dataset;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.tdb.TDBFactory;

import java.util.*;

public class DataContainer implements ThreadCompleteListener {

    Set<String> myFrontier = new HashSet<>(10000);
    Set<String> myBlackList = new HashSet<>(300000);
    Set<Statement> myModel = new HashSet<>(300000);

    private final Dataset DS;
    private final Model TDB;
    private final int myThreadCount;
    private Queue<Result4Frontier> myFrontiers = new LinkedList<>();
    private Result4Frontier myFrontierWorker = null;

    private Queue<Result4Model> myModels = new LinkedList<>();
    private Result4Model myModelWorker = null;

    public DataContainer(String _seed, int _threads) {
        myFrontier.add(_seed);
        myBlackList.add(_seed);
        DS = TDBFactory.createDataset("TDBStore");
        TDB = DS.getDefaultModel();
        myThreadCount = _threads;
    }

    public synchronized void InsertStatement(List<Statement> _list, boolean _inbound) {
        Result4Frontier newResult = new Result4Frontier(_list, _inbound, myFrontier, myBlackList, this);

//        try {
//            DS.begin(ReadWrite.WRITE);
//            TDB.add(_list);
//            TDB.commit();
//            DS.end();
//        } catch (Exception e) {
//            System.out.println(e.getMessage());
//        }
    }

    private synchronized void Process(
            WorkerType _type,
            List<Statement> _data,
            boolean _inbound) {
        switch (_type) {
            case FetcherWorker:
                Result4Frontier newFrontier = new Result4Frontier(_data, _inbound, myFrontier, myBlackList, this);
                if (myFrontierWorker == null) {
                    newFrontier.start();
                    myFrontierWorker = newFrontier;
                } else if (!myFrontierWorker.isAlive()) {
                    newFrontier.start();
                    myFrontierWorker = newFrontier;
                } else {
                    myFrontiers.add(newFrontier);
                }
                break;
            case FrontierWorker:
                if (!myFrontiers.isEmpty()) {
                    myFrontierWorker = myFrontiers.remove();
                    myFrontierWorker.start();
                }
                break;
            case ModelWorker:
                if (!myModels.isEmpty()) {
                    myModelWorker = myModels.remove();
                    myModelWorker.start();
                }
                break;
        }
    }

    public String[] getFrontier() {
        String[] frontierNodes = new String[myFrontier.size()];
        myFrontier.toArray(frontierNodes);
        return frontierNodes;
    }

    public void clearFrontier() {
        myFrontier.clear();
    }

    public void close() {
        TDB.close();
        DS.close();
    }

    @Override
    public void notificationThreadComplete(Thread thread) {

        if (thread instanceof Result4Model) {
            Process(WorkerType.ModelWorker, null, true);
        } else if (thread instanceof Result4Frontier) {
            Process(WorkerType.FrontierWorker, null, true);
        } else {
            // Shit has gone wrong, who called me??
        }
    }
}
