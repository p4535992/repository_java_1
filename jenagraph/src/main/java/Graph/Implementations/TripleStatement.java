/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Graph.Implementations;

import java.util.Objects;

public class TripleStatement {
    
    public String mySubject;
    public String mySubjectShort;
    public boolean SubjectIsBlank = false;
    
    public String myPredicate;
    public String myPredicateShort;
    
    public String myObject;
    public String myObjectShort;
    public boolean ObjectIsBlank = false;
    public boolean ObjectIsLiteral = false;

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.mySubject);
        hash = 53 * hash + Objects.hashCode(this.myPredicate);
        hash = 53 * hash + Objects.hashCode(this.myObject);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TripleStatement other = (TripleStatement) obj;
        return Objects.equals(this.mySubject, other.mySubject) &&
                Objects.equals(this.myPredicate, other.myPredicate) &&
                Objects.equals(this.myObject, other.myObject);
    }
}
