/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/


package Debugging;

import Graph.Algorithm.PBFS.TDB;
import Graph.Algorithm.PBFS.TDBManager;
import Graph.Algorithm.PBFS.DataManager;
import Graph.Algorithm.PBFS.DataWorker;
import Graph.Algorithm.PBFS.MetaWorker;
import Graph.Implementations.StopWatch;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Debugging3 {

    public static void main(String args[]) {
        List<String> list = new ArrayList<>();
        list.add("http://dbpedia.org/resource/Albert_Einstein");

        String endpoint = "http://dbpedia.org/sparql";
        String[] filters = {"http://www.w3.org/2002/07/owl#", "http://www.w3.org/2000/01/rdf-schema#",
            "http://www.w3.org/1999/02/22-rdf-syntax-ns#", "http://www.w3.org/2001/sw/RDFCore/Schema/200203/rdfs-namespace.xml",
            "http://dbpedia.org/class/yago/"};

        DataManager DM = new DataManager("http://dbpedia.org/resource/Albert_Einstein", filters);
        int threads = 8;
        TDB tdb = TDBManager.CreateTDB("http://dbpedia.org/resource/Albert_Einstein", endpoint, 0, filters);
        tdb.Connect();
        MetaWorker[] metaWorkers = new MetaWorker[threads];
        DataWorker[] dataWorkers = new DataWorker[threads];
        StopWatch sw = new StopWatch();
        
        for (int i = 1; i <= 3; i++) {
            tdb.IncrementLength();
            sw.start();
            for (int j = 0; j < metaWorkers.length; j++) {
                metaWorkers[j] = new MetaWorker(endpoint, DM);
                metaWorkers[j].start();
            }

            for (MetaWorker mw : metaWorkers) {
                try {
                    mw.join();
                } catch (InterruptedException ex) {
                    Logger.getLogger(Debugging3.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            for(int j = 0; j < dataWorkers.length; j++) {
                dataWorkers[j] = new DataWorker(endpoint, DM, tdb);
                dataWorkers[j].start();
            }

            for (DataWorker dw : dataWorkers) {
                try {
                    dw.join();
                } catch (Exception ignored) {

                }
            }

            DM.MoveFrontier();
            sw.stop();
            System.out.println(sw.getElapsedTime());
        }
        
        tdb.WriteFrontier(DM.getFrontier());
        tdb.Close();
        list.clear();
    }
}
