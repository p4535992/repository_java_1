/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Debugging;

import Graph.Algorithm.PBFS.TDB;
import Graph.Algorithm.PBFS.TDBManager;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.shared.LockMRSW;

public class Debugging1 {

    public static void main(String args[]) {
//        List<String> list = new ArrayList<>();
//        File file = new File("URIs.txt");
//        try {
//            FileReader fr = new FileReader(file);
//            BufferedReader br = new BufferedReader(fr);
//            String line = br.readLine();
//            
//            while(line != null) {
//                list.add(line);
//                line = br.readLine();
//            }
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(Debugging1.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (IOException ex) {
//            Logger.getLogger(Debugging1.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        
//        
//        String query = SPARQLFactory.GetCount(list, null);
//        Query q = QueryFactory.create(query);
//        QueryExecution qExe
//                = QueryExecutionFactory.sparqlService("http://dbpedia.org/sparql/", q);
//
//        int counter = 0;
//        try {
//            ResultSet rs = qExe.execSelect();
//            while (rs.hasNext()) {
//                rs.next();
//                counter++;
//            }
//            qExe.close();
//        } catch (Exception e) {
//            System.out.println(e.toString());
//        }
//        System.out.println(counter);
        String[] filters = new String[]{"http://www.w3.org/2002/07/owl#",
            "http://www.w3.org/2000/01/rdf-schema#",
            "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
            "http://dbpedia.org/class/yago/"};
        TDB tdb = TDBManager.getModel("http://dbpedia.org/resource/Albert_Einstein", "http://dbpedia.org/sparql", filters);
        
        tdb.Connect();
        Model m = tdb.OpenModel();
        
        m.enterCriticalSection(LockMRSW.READ);
        try {
            m.createResource("http://zebra.com");
            m.commit();
        } catch (Exception e) {
            System.out.println(e.toString());
        } finally {
            m.leaveCriticalSection();
        }
    }
}
