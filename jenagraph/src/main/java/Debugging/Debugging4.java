/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Debugging;

import Graph.Algorithm.PBFS.TDB;
import Graph.Algorithm.PBFS.TDBManager;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Elipson
 */
public class Debugging4 {

    public static void main(String args[]) {
        List<String> list = new ArrayList<>();
        list.add("http://dbpedia.org/resource/Albert_Einstein");

        String endpoint = "http://dbpedia.org/sparql";
        String[] filters = {"http://www.w3.org/2002/07/owl#", "http://www.w3.org/2000/01/rdf-schema#",
            "http://www.w3.org/1999/02/22-rdf-syntax-ns#", "http://www.w3.org/2001/sw/RDFCore/Schema/200203/rdfs-namespace.xml",
            "http://dbpedia.org/class/yago/"};

        TDB tdb = TDBManager.getModel("http://dbpedia.org/resource/Albert_Einstein", endpoint, filters);
        
        String q = "SELECT COUNT(*) WHERE {?s ?p ?o}";
        tdb.performSelect(q);
    }
}
