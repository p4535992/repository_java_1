/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package Annotation;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Score {

    public String myURL;
    public List<Boolean> myScores = new ArrayList<>();
    public boolean isDone = false;
    
    public Score(String _url, List<Boolean> _scores, boolean _isDone) {
        myURL = _url;
        myScores = _scores;
        isDone = _isDone;
    }

    /**
     * calculates the normalized score
     * @return Returns double [0;1]
     */
    public double CalcScore() {
        double result = 0.0;
        for (boolean b : myScores) {
            if (b == true) {
                result++;
            }
        }
        result = result / ((double) myScores.size());
        return result;
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.myURL);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Score other = (Score) obj;
        if (!Objects.equals(this.myURL, other.myURL)) {
            return false;
        }
        return true;
    }
}
