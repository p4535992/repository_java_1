/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package DataSources;

import Graph.InterestingSearch.InterestingGraphSearch;
import GUI.Threads.InterestingGraphThread;
import GUI.Threads.InterestingPathsThread;
import Graph.Algorithm.PBFS.TDB;
import Graph.Algorithm.PBFS.TDBManager;
import Graph.Enumerator.DeleteEdge;
import Graph.Enumerator.DeleteLiteral;
import Graph.Enumerator.DeleteVertex;
import Graph.Enumerator.InsertLiteral;
import Graph.Enumerator.InterestingResult;
import Graph.Enumerator.NodeType;
import Graph.Enumerator.ReadyToVisualise;
import Graph.Enumerator.UpdateLiteralText;
import Graph.Enumerator.VisualiseStatus;
import Graph.Implementations.GraphHelperUtil;
import Graph.Implementations.ResourceResult;
import Graph.Implementations.SPARQLFactory;
import Graph.Implementations.SPARQL_ResultSet;
import Graph.Implementations.TripleStatement;
import Graph.Implementations.TripleStatementWrapper;
import Graph.Implementations.VisualiseWrapper;
import Graph.InterestingSearch.InterestingLiterals;
import Graph.Interfaces.DataSourceInterface;
import Graph.InterestingPaths.DisambiguityResult;
import Graph.InterestingPaths.DisambiguityResults;
import Graph.InterestingPaths.InterestingPath;
import Graph.InterestingSearch.LiteralTriple;
import Graph.InterestingSearch.RDFTree;
import Graph.InterestingSearch.RDFTreeNode;
import Graph.InterestingSearch.WordScorer;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import org.apache.jena.graph.Node;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.*;
import org.apache.jena.sparql.engine.http.QueryExceptionHTTP;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GenericHTTPEndPoint implements DataSourceInterface {

    private String myURL;
    private Model myCurrentGraph;
    private int maxVertices = 300;
    private String myCurrentCenter = "";
    private NodeType myCenterType = null;
    private int myTimeout = 30000;

    /**
     *
     * @param _URL URL of Endpoint being accessed
     */
    public GenericHTTPEndPoint(String _URL) {
        this.myURL = _URL;
    }

    /**
     *
     * @param _query select statement to be executed
     * @return returns SPARQL_ResultSet which contains Exception information, if
     * one was encountered
     */
    @Override
    public SPARQL_ResultSet performSelect(String _query) {
        SPARQL_ResultSet resultSet = new SPARQL_ResultSet();

        try {
            Query query = QueryFactory.create(_query);

            QueryExecution qExe = QueryExecutionFactory.sparqlService(this.myURL, query);
            qExe.setTimeout(myTimeout);
            ResultSet results = qExe.execSelect();

            resultSet.myHeaders = new Vector<String>(results.getResultVars());
            int stringLength = resultSet.myHeaders.size();

            resultSet.myData = new Vector<Vector<String>>();

            while (results.hasNext()) {
                Vector<String> vector = new Vector<String>();
                QuerySolution solution = results.next();

                for (int j = 0; j < stringLength; j++) {
                    vector.add(solution.get(resultSet.myHeaders.get(j)).toString());
                }
                resultSet.myData.add(vector);

                resultSet.validSet = true;
            }

            // Clean up
            qExe.close();
        } catch (Exception e) {
            resultSet.validSet = false;
            resultSet.errorMessage = e.toString();
        }
        return resultSet;
    }

    /**
     *
     * @param _query construct statement to be executed
     * @return returns Jena Model resulting from expression
     */
    @Override
    public Model performConstruct(String _query) throws QueryExceptionHTTP {
        Query myQuery = QueryFactory.create(_query);
        QueryExecution qExe = QueryExecutionFactory.sparqlService(this.myURL, myQuery);
        qExe.setTimeout(myTimeout);
        return qExe.execConstruct();
    }

    /**
     *
     * @param _query Statement to be executed
     * @return Returns boolean result
     * @throws QueryExceptionHTTP throw if any error is occurred.
     */
    @Override
    public boolean performAsk(String _query) throws QueryExceptionHTTP {
        Query myQuery = QueryFactory.create(_query);
        QueryExecution qExe = QueryExecutionFactory.sparqlService(this.myURL, myQuery);
        qExe.setTimeout(myTimeout);
        boolean result = qExe.execAsk();
        qExe.close();
        return result;
    }

    /**
     * Runs a simple ASK { } query
     *
     * @return Returns true if connection was successful, false otherwise
     */
    @Override
    public boolean verifyConnection() {
        // SPARQL query used for testing connection
        String q = "ASK { }";

        try {
            Query myQuery = QueryFactory.create(q);
            QueryExecution qExe = QueryExecutionFactory.sparqlService(this.myURL, myQuery);
            qExe.setTimeout(myTimeout);
            boolean result = qExe.execAsk();

            qExe.close();
            return result;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public ReadyToVisualise ReadyToVisualise() {
        if (this.myCurrentCenter.length() == 0) {
            return ReadyToVisualise.NeedsFocalPoint;
        } else {
            return ReadyToVisualise.Ready;
        }
    }

    @Override
    public ArrayList<TripleStatement> GetStatements() {
        ArrayList<TripleStatement> myStatements = new ArrayList<>();

        TripleStatement temp;
        StmtIterator itr = this.myCurrentGraph.listStatements();

        while (itr.hasNext()) {
            temp = new TripleStatement();
            Statement stmt = itr.nextStatement();

            Resource subject = stmt.getSubject();
            temp.mySubject = subject.toString();
            if (subject.asNode().isBlank()) {

                temp.SubjectIsBlank = true;
                temp.mySubjectShort = "BN";
            } else {
                temp.mySubjectShort = GraphHelperUtil.cropURI(temp.mySubject);
            }

            Property predicate = stmt.getPredicate();
            temp.myPredicate = predicate.toString();
            temp.myPredicateShort = GraphHelperUtil.cropURI(temp.myPredicate);

            RDFNode myNode = stmt.getObject();
            temp.myObject = myNode.toString();
            if (myNode.isLiteral()) {
                temp.ObjectIsLiteral = true;
                temp.myObjectShort = GraphHelperUtil.cropLiteral(temp.myObject);
            } else if (myNode.asNode().isBlank()) {
                temp.myObjectShort = "BN";
                temp.ObjectIsBlank = true;
            } else {
                temp.myObjectShort = GraphHelperUtil.cropURI(temp.myObject);
            }

            myStatements.add(temp);
        }
        return myStatements;
    }

    @Override
    public void UpdateModel() throws QueryExceptionHTTP {
        String constStmt = null;
        if (myCenterType == NodeType.Vertex || myCenterType == NodeType.BlankNode) {
            constStmt = String.format("CONSTRUCT { \n"
                    + "<%s> ?p ?o .\n"
                    + "?s ?p <%s> \n"
                    + "} WHERE {\n"
                    + "{<%s> ?p ?o }\n"
                    + "UNION\n"
                    + "{?s ?p <%s> }\n"
                    + "} LIMIT %d",
                    myCurrentCenter, myCurrentCenter,
                    myCurrentCenter, myCurrentCenter, maxVertices);
        } else if (myCenterType == NodeType.Literal) {
            // Escaping quotation marks
            String center;
            center = myCurrentCenter.replace("\"", "\\\"");

            // Inserting into expressions
            constStmt = String.format("CONSTRUCT {\n"
                    + "?s ?p \"%s\"\n"
                    + "} \n"
                    + "WHERE {\n"
                    + "{?s ?p \"%s\" }\n"
                    + "} LIMIT %d", center, center,
                    maxVertices);
        }
        this.myCurrentGraph = this.performConstruct(constStmt);
    }

    /**
     * Updates the current local model for this endpoint Once an update has been
     * performed, the endpoint can be visualised.
     *
     * @param _newCenter New center to used as focal point
     * @throws QueryExceptionHTTP throw if any http error is occurred.
     */
    @Override
    public void UpdateModel(String _newCenter, NodeType _type) throws QueryExceptionHTTP {
        this.myCurrentCenter = _newCenter;
        this.myCenterType = _type;

        String constStmt = null;
        if (myCenterType == NodeType.Vertex || myCenterType == NodeType.BlankNode) {
            constStmt = String.format("CONSTRUCT { \n"
                    + "<%s> ?p ?o .\n"
                    + "?s ?p <%s> \n"
                    + "} WHERE {\n"
                    + "{<%s> ?p ?o }\n"
                    + "UNION\n"
                    + "{?s ?p <%s> }\n"
                    + "} LIMIT %d",
                    myCurrentCenter, myCurrentCenter,
                    myCurrentCenter, myCurrentCenter, maxVertices);
        } else if (myCenterType == NodeType.Literal) {
            // Escaping quotation marks
            String center;
            center = myCurrentCenter.replace("\"", "\\\"");

            String pattern = "\\@([a-z]|[A-Z])+(\\-\\w+)*$";
            Pattern pat = Pattern.compile(pattern);
            Matcher mat = pat.matcher(center);

            int count = 0;

            while (mat.find()) {
                count++;
            }

            // Language detected
            if (count == 1) {
                if(mat.find(0)) {
                    // Adding quotation marks and moving @lang outside of
                    // quotation marks.
                    center = "\"" + center.replace(mat.group(), "") + "\"" + mat.group();
                }else{
                    center = "\"" + center + "\"";
                }
            } // No language detected
            else {
                center = "\"" + center + "\"";
            }

            // Inserting into expressions
            constStmt = String.format("CONSTRUCT {\n"
                    + "?s ?p %s\n"
                    + "} \n"
                    + "WHERE {\n"
                    + "?s ?p %s .\n"
                    + "} LIMIT %d", center, center,
                    maxVertices);
        }
        this.myCurrentGraph = this.performConstruct(constStmt);
    }

    @Override
    public void setCenterVertex(String _newCenter, NodeType _type) {
        this.myCurrentCenter = _newCenter;
        this.myCenterType = _type;
    }

    @Override
    public String getCenterVertex() {
        return this.myCurrentCenter;
    }

    @Override
    public Graph.Enumerator.DeleteVertex DeleteVertex(String _node) {
        return DeleteVertex.ChangeDenied;
    }

    @Override
    public ResourceResult InsertVertex(String _value) {
        return null;
    }

    public boolean ContainsResource(String _ID) {
        return false;
    }

    @Override
    public ResourceResult UpdateVertexURI(String _vertex, String _newURI) {
        return null;
    }

    @Override
    public ResourceResult VertexToBlankNode(String _vertex) {
        return null;
    }

    @Override
    public ResourceResult ResolveBlankNode(String _nodeID, String _newURI) {
        return null;
    }

    @Override
    public Graph.Enumerator.DeleteVertex DeleteBlankNode(String _nodeID) {
        return DeleteVertex.ChangeDenied;
    }

    @Override
    public ResourceResult InsertBlankNode() {
        return null;
    }

    @Override
    public Graph.Enumerator.UpdateLiteralText UpdateLiteralText(String _literal, String _newText) {
        return UpdateLiteralText.ChangeDenied;
    }

    @Override
    public Graph.Enumerator.DeleteLiteral DeleteLiteral(String _literal) {
        return DeleteLiteral.ChangeDenied;
    }

    @Override
    public Graph.Enumerator.InsertLiteral InsertLiteral(String _text) {
        return InsertLiteral.ChangeDenied;
    }

    @Override
    public ResourceResult ChangeEdgeURI(String _edgeURI, String _source, NodeType _sourceType, String _target, NodeType _targetType, String _newEdgeURI) {
        return null;
    }

    @Override
    public ResourceResult InsertEdge(String _edgeURI, String _source, NodeType _sourceType, String _target, NodeType _targetType) {
        return null;
    }

    @Override
    public Graph.Enumerator.DeleteEdge DeleteEdge(String _model, String _edgeURI, String _source, NodeType _sourceType, String _target, NodeType _targetType) {
        return DeleteEdge.ChangeDenied;
    }

    @Override
    public NodeType getNodeType(String _value) {
        Resource node = myCurrentGraph.getResource(_value);
        if (node.isAnon()) {
            return NodeType.BlankNode;
        } else if (node.isLiteral()) {
            return NodeType.Literal;
        } else if (node.isURIResource()) {
            return NodeType.Vertex;
        } else {
            return null;
        }
    }

    @Override
    public ArrayList<TripleStatement> CBD(String _vertex) {
        //ArrayList<TripleStatement> result = new ArrayList<>();
        int neighbourCount = 1;
        boolean keepLooking = true;
        // Loading k = 1 neighbours
        Model currentModel = getNeighboursAt(neighbourCount, _vertex);
        Model nextModel;
        neighbourCount++;

        if(currentModel == null) return new ArrayList<>();

        while (keepLooking) {
            nextModel = getNeighboursAt(neighbourCount, _vertex);

            keepLooking = nextModel != null &&
                    nextModel.size() < 2 * maxVertices && nextModel.size() != currentModel.size();
            currentModel = nextModel;
            neighbourCount++;
        }

        if(currentModel == null) return new ArrayList<>();

        // As the constructed model sometimes contains
        // erroniously selected nodes, filtering is applied.
        Model cleanModel = ModelFactory.createDefaultModel();
        Resource c = currentModel.getResource(_vertex);
        Queue<Resource> frontier = new LinkedList<>();
        frontier.add(c);
        List<Statement> stmts;
        Resource center;

        while (frontier.size() > 0
                && cleanModel.size() < maxVertices) {
            center = frontier.remove();
            stmts = currentModel.listStatements(center, null,
                    (RDFNode) null).toList();
            for (Statement s : stmts) {
                cleanModel.add(s);
                RDFNode obj = s.getObject();
                if (obj.isAnon()) {
                    frontier.add(obj.asResource());
                }
            }
        }
        return getTriples(cleanModel);
    }

    @Override
    public ArrayList<TripleStatement> SCBD(String _literal) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private Model getNeighboursAt(int _k, String _c) {
        StringBuilder query = new StringBuilder();
        if (_k == 1) {
            query.append(String.format(
                    "CONSTRUCT {\n"
                    + "<%s> ?p1 ?o1.\n"
                    + "}\n"
                    + "WHERE {\n"
                    + "<%s> ?p1 ?o1.\n"
                    + "}\n", _c, _c));
        } else if (_k > 1) {
            query.append(String.format("CONSTRUCT {\n"
                    + "<%s> ?p1 ?o1.\n", _c));

            for (int i = 2; i <= _k; i++) {
                query.append(String.format("?o%s ?p%s ?o%s.\n", (i - 1), i, i));
            }
            query.append(String.format("}\n"
                    + "WHERE {\n"
                    + "{<%s> ?p1 ?o1.}\n", _c));

            for (int i = 2; i <= _k; i++) {
                query.append(String.format(
                        "UNION\n"
                        + "{FILTER(isBlank(?o%s)).\n"
                        + "?o%s ?p%s ?o%s.\n"
                        + "}\n", (i - 1), (i - 1), i, i));
            }
            query.append("}");
        }

        if (!query.toString().isEmpty()) {
            try {
                return this.performConstruct(query.toString());
            } catch (Exception e) {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Iterates over a model and returns an ArrayList of triples for use during
     * visualisation
     *
     * @param _m Model to be iterated over
     * @return Returns an empty list for an empty model, otherwise returns
     * filled ArrayList
     */
    private ArrayList<TripleStatement> getTriples(Model _m) {
        ArrayList<TripleStatement> result = new ArrayList<>();
        TripleStatement tempStmt;
        StmtIterator itr = _m.listStatements();

        while (itr.hasNext()) {
            tempStmt = new TripleStatement();
            Statement stmt = itr.nextStatement();

            Resource subject = stmt.getSubject();
            tempStmt.mySubject = subject.toString();
            if (subject.asNode().isBlank()) {
                tempStmt.SubjectIsBlank = true;
                tempStmt.mySubjectShort = "BN";
            } else {
                tempStmt.mySubjectShort = GraphHelperUtil.cropURI(tempStmt.mySubject);
            }

            Property predicate = stmt.getPredicate();
            tempStmt.myPredicate = predicate.toString();
            tempStmt.myPredicateShort = GraphHelperUtil.cropURI(tempStmt.myPredicate);

            RDFNode myNode = stmt.getObject();
            tempStmt.myObject = myNode.toString();
            if (myNode.isLiteral()) {
                tempStmt.ObjectIsLiteral = true;
                tempStmt.myObjectShort = GraphHelperUtil.cropLiteral(tempStmt.myObject);
            } else if (myNode.asNode().isBlank()) {
                tempStmt.myObjectShort = "BN";
                tempStmt.ObjectIsBlank = true;
            } else {
                tempStmt.myObjectShort = GraphHelperUtil.cropURI(tempStmt.myObject);
            }
            result.add(tempStmt);
        }
        return result;
    }

    @Override
    public TripleStatementWrapper InterestingSearch(String _bagOfWords, int _maxNodes, int _maxDist, String _initNode, NodeType _initType, double _beta, String[] _filters, InterestingGraphThread _report, String _lang) {

        TDB tdb;
        TripleStatementWrapper result = new TripleStatementWrapper();
        try {
            if (TDBManager.containsModel(_initNode, myURL, _filters)) {
                tdb = TDBManager.getModel(_initNode, myURL, _filters);
                int size = tdb.getMyLength();

                if (size < _maxDist) {
                    _report.ReportProgress("TDBStore contains partial model. Expanding model by " + (_maxDist - size) + " steps");
                    TDBManager.ExpandTDB(_maxDist - size, tdb);
                } else {
                    _report.ReportProgress("TDBStore contains model of sufficient size and settings. Using this model...");
                }
            } else {
                _report.ReportProgress("TDBStore contains no suitable model. Building new model.");
                tdb = TDBManager.CreateTDB(_initNode, myURL, _maxDist, _filters);
            }

            tdb.Connect();
            Model m = tdb.OpenModel();
            InterestingGraphSearch mySearcher = new InterestingGraphSearch(this, m, _bagOfWords, _initNode, _lang, _maxNodes, _maxDist, _beta, _filters, _report);
            result = mySearcher.InterestingSearch();

            m.close();
            tdb.CloseModel();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return result;
    }

    private void processNeighbours(Model _m,
            RDFTreeNode _c,
            PriorityQueue<RDFTreeNode> _frontier,
            RDFTree _tree,
            HashSet<RDFNode> _processed,
            WordScorer _scorer) {

        RDFNode curr = _c.getMyValue();
        Statement stmt;

        // Finding all stmts where _c is sbj
        StmtIterator stmts = _m.listStatements(curr.asResource(), null, (RDFNode) null);

        while (stmts.hasNext()) {
            stmt = stmts.next();
            RDFNode obj = stmt.getObject();

            if (!_processed.contains(obj) && !obj.isLiteral()) {
                RDFTreeNode temp = new RDFTreeNode(obj);
                //double score = scoreNode(obj, _scorer);
                //temp.setMyMatchScore(score);

                // Adding to tree
                if (!_tree.contains(temp)) {
                    _tree.addNode(_c, temp);
                }

                // Adding to frontier
                if (!_frontier.contains(temp)) {
                    _frontier.add(temp);
                }
            }
        }

        stmts = _m.listStatements(null, null, curr);

        while (stmts.hasNext()) {
            stmt = stmts.next();
            RDFNode sbj = stmt.getSubject();

            if (!_processed.contains(sbj) && !sbj.isLiteral()) {
                RDFTreeNode temp = new RDFTreeNode(sbj);
                //double score = scoreNode(sbj, _scorer);

                //temp.setMyMatchScore(score);
                // Adding to tree
                if (!_tree.contains(temp)) {
                    _tree.addNode(_c, temp);
                }

                // Adding to frontier
                if (!_frontier.contains(temp)) {
                    _frontier.add(temp);
                }
            }
        }
    }

    @Override
    public InterestingLiterals getLiterals(String _center, NodeType _centerType) {
        InterestingLiterals result = new InterestingLiterals();
        
        String query = String.format("SELECT (<%s> AS ?known) ?p ?o WHERE "
                + "{"
                + "<%s> ?p ?o ."
                + "FILTER(isLiteral(?o)) ."
                + "}", _center, _center);
        SPARQL_ResultSet r = performSelect(query);
        
        for(Vector<String> v : r.myData) {
            String sbj = v.get(0);
            String pred = v.get(1);
            String predShort = GraphHelperUtil.cropURI(v.get(1));
            String lit = v.get(2);
            String litShort = GraphHelperUtil.cropLiteral(v.get(2));
            result.myLiteral.add(new LiteralTriple(sbj, pred, predShort, lit, litShort));
        }
        result.actionReport = InterestingResult.Success;
        return result;
    }

    @Override
    public int getMaxVertices() {
        return maxVertices;
    }

    @Override
    public void setMaxVertices(int _max) {
        this.maxVertices = _max;
    }

    @Override
    public VisualiseWrapper CenterOn(String _center, NodeType _type) {
        return CreateVisualiseSubGraph(_center);
    }

    @Override
    public VisualiseWrapper CenterOn() {
        return CreateVisualiseSubGraph(this.myCurrentCenter);
    }

    /**
     * Creates a sub-graph around _init with a distance of two
     *
     * @param _init Node to be center
     * @return Model containing sub-graph
     */
    private VisualiseWrapper CreateVisualiseSubGraph(String _init) {
        VisualiseWrapper result = new VisualiseWrapper();
        Model model;

        String query = String.format(
                "CONSTRUCT {\n"
                + "	# k = 1\n"
                + "	<%s> ?p11 ?o11.\n"
                + "	?s12 ?p12 <%s>.\n"
                + "}\n"
                + "WHERE {\n"
                + "	# k = 1\n"
                + "	<%s> ?p11 ?o11.\n"
                + "	?s12 ?p12 <%s>.\n"
                + "}", _init, _init, _init, _init);

        try {
            model = performConstruct(query);
        } catch (QueryExceptionHTTP e) {
            // We got an error during 
            result.myTriples = new ArrayList<>();
            result.myStatus = VisualiseStatus.Timeout;
            return result;
        }

        if (size(model) > maxVertices) {
            result.myTriples = getStatements(model);
            result.myStatus = VisualiseStatus.Success;
            return result;
        }

        query = String.format(
                "CONSTRUCT {\n"
                + "	# k = 1\n"
                + "	<%s> ?p11 ?o11.\n"
                + "	?s12 ?p12 <%s>.\n"
                + "	\n"
                + "	# k = 2 - Out\n"
                + "	?o11 ?p21 ?o21.\n"
                + "	?s21 ?p_21 ?o11.\n"
                + "	\n"
                + "	# k = 2 - In\n"
                + "	?s12 ?p22 ?o22.\n"
                + "	?s22 ?p_22 ?s12.\n"
                + "}\n"
                + "WHERE {\n"
                + "	# k = 1\n"
                + "	<%s> ?p11 ?o11.\n"
                + "	?s12 ?p12 <%s>.\n"
                + "	\n"
                + "	# k = 2 - Out\n"
                + "	?o11 ?p21 ?o21.\n"
                + "	?s21 ?p_21 ?o11.\n"
                + "	\n"
                + "	# k = 2 - In\n"
                + "	?s12 ?p22 ?o22.\n"
                + "	?s22 ?p_22 ?s12.\n"
                + "}", _init, _init, _init, _init);

        Model temp;
        try {
            temp = performConstruct(query);
        } catch (QueryExceptionHTTP e) {
            // Encountered an error, returning previous model
            result.myTriples = getStatements(model);
            result.myStatus = VisualiseStatus.Timeout;
            return result;
        }

        if (size(temp) > maxVertices) {
            result.myTriples = getStatements(temp);
            result.myStatus = VisualiseStatus.Success;
            return result;
        } else {
            // Our new best candidate model
            model = temp;
        }

        query = String.format("CONSTRUCT {\n"
                + "	# k = 1\n"
                + "	<%s> ?p11 ?o11.\n"
                + "	?s12 ?p12 <%s>.\n"
                + "	\n"
                + "	# k = 2 - Out\n"
                + "	?o11 ?p21 ?o21.\n"
                + "	?s21 ?p_21 ?o11.\n"
                + "	\n"
                + "	# k = 2 - In\n"
                + "	?s12 ?p22 ?o22.\n"
                + "	?s22 ?p_22 ?s12.\n"
                + "	\n"
                + "	# k = 3 - Out\n"
                + "	?o21 ?p31 ?o31.\n"
                + "	?s31 ?p_31 ?o21.\n"
                + "	# k = 3 - Out\n"
                + "	?s21 ?p__31 ?o_31.\n"
                + "	?s31 ?p___31 ?s_21.\n"
                + "	\n"
                + "	# k = 3 - In\n"
                + "	?o22 ?p32 ?o32.\n"
                + "	?s32 ?p_32 ?o22.\n"
                + "	# k= 3 - In\n"
                + "	?s22 ?p__32 ?o32.\n"
                + "	?s31 ?p___32 ?s22.\n"
                + "}\n"
                + "WHERE {\n"
                + "	# k = 1\n"
                + "	<%s> ?p11 ?o11.\n"
                + "	?s12 ?p12 <%s>.\n"
                + "	\n"
                + "	# k = 2 - Out\n"
                + "	?o11 ?p21 ?o21.\n"
                + "	?s21 ?p_21 ?o11.\n"
                + "	\n"
                + "	# k = 2 - In\n"
                + "	?s12 ?p22 ?o22.\n"
                + "	?s22 ?p_22 ?s12.\n"
                + "	\n"
                + "	# k = 3 - Out\n"
                + "	?o21 ?p31 ?o31.\n"
                + "	?s31 ?p_31 ?o21.\n"
                + "	# k = 3 - Out\n"
                + "	?s21 ?p__31 ?o_31.\n"
                + "	?s31 ?p___31 ?s_21.\n"
                + "	\n"
                + "	# k = 3 - In\n"
                + "	?o22 ?p32 ?o32.\n"
                + "	?s32 ?p_32 ?o22.\n"
                + "	# k= 3 - In\n"
                + "	?s22 ?p__32 ?o32.\n"
                + "	?s31 ?p___32 ?s22.\n"
                + "}", _init, _init, _init, _init);

        try {
            temp = performConstruct(query);
            result.myTriples = getStatements(temp);
            result.myStatus = VisualiseStatus.Success;
            return result;
        } catch (QueryExceptionHTTP e) {
            result.myTriples = getStatements(model);
            result.myStatus = VisualiseStatus.Timeout;
            return result;
        }
    }

    /**
     * Returns the number of entities for the given model
     *
     * @param _model Model to explore
     * @return Number of entities or -1 if error
     */
    private int size(Model _model) {
        String countQuery = "SELECT (COUNT(*) AS ?count) {\n"
                + "   SELECT DISTINCT ?s ?o {\n"
                + "     ?s ?p ?o.\n"
                + "   }\n"
                + "}";
        try {
            Query query = QueryFactory.create(countQuery);

            QueryExecution qExe = QueryExecutionFactory.create(query, _model);
            ResultSet results = qExe.execSelect();

            QuerySolution solution = results.next();
            return solution.get("count").asLiteral().getInt();
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    public ArrayList<TripleStatement> getStatements(Model _m) {
        ArrayList<TripleStatement> myStatements = new ArrayList<>();
        Set<Node> distinctNodes = new HashSet<>();
        TripleStatement temp;
        StmtIterator itr = _m.listStatements();

        while (itr.hasNext() && (distinctNodes.size() < maxVertices)) {
            temp = new TripleStatement();
            Statement stmt = itr.nextStatement();

            Resource subject = stmt.getSubject();
            if (!distinctNodes.contains(subject.asNode())) {
                distinctNodes.add(subject.asNode());
            }
            temp.mySubject = subject.toString();
            if (subject.asNode().isBlank()) {
                temp.SubjectIsBlank = true;
                temp.mySubjectShort = "BN";
            } else {
                temp.mySubjectShort = GraphHelperUtil.cropURI(subject.toString(), _m.getNsPrefixMap());
            }

            Property predicate = stmt.getPredicate();
            temp.myPredicate = predicate.toString();
            temp.myPredicateShort = GraphHelperUtil.cropURI(predicate.toString(), _m.getNsPrefixMap());

            RDFNode myNode = stmt.getObject();
            if (!distinctNodes.contains(myNode.asNode())) {
                distinctNodes.add(myNode.asNode());
            }
            temp.myObject = myNode.toString();
            if (myNode.isLiteral()) {
                temp.ObjectIsLiteral = true;
                temp.myObjectShort = GraphHelperUtil.cropLiteral(temp.myObject);
            } else if (myNode.asNode().isBlank()) {
                temp.myObjectShort = "BN";
                temp.ObjectIsBlank = true;
            } else {
                temp.myObjectShort = GraphHelperUtil.cropURI(myNode.toString(), _m.getNsPrefixMap());
            }

            myStatements.add(temp);
        }
        return myStatements;
    }

    @Override
    public DisambiguityResults DisambiguitySearch(String _searchTerm, String _lang) {
        DisambiguityResults result;

        String q;
        if (_lang.length() > 0) {
            q = String.format(
                    "SELECT ?s ?l (count(?s) AS ?count) WHERE {\n"
                    + "?someobj ?p ?s .\n"
                    + "?s <http://www.w3.org/2000/01/rdf-schema#label> ?l .\n"
                    + "?l <bif:contains> '\"%s\"' .\n"
                    + "FILTER (!regex(str(?s), '^http://dbpedia.org/resource/Category:')).\n"
                    + "FILTER (!regex(str(?s), '^http://dbpedia.org/resource/List')).\n"
                    + "FILTER (!regex(str(?s), '^http://sw.opencyc.org/')).\n"
                    + "FILTER (lang(?l) = '%s').\n"
                    + "FILTER (!isLiteral(?someobj)).\n"
                    + "} GROUP BY ?s ?l ORDER BY DESC(?count) LIMIT 20", _searchTerm, _lang);
        } else {
            q = String.format(
                    "SELECT ?s ?l (count(?s) AS ?count) WHERE {\n"
                    + "?someobj ?p ?s .\n"
                    + "?s <http://www.w3.org/2000/01/rdf-schema#label> ?l .\n"
                    + "?l <bif:contains> '%s' .\n"
                    + "FILTER (!regex(str(?s), '^http://dbpedia.org/resource/Category:')).\n"
                    + "FILTER (!regex(str(?s), '^http://dbpedia.org/resource/List')).\n"
                    + "FILTER (!regex(str(?s), '^http://sw.opencyc.org/')).\n"
                    + "FILTER (!isLiteral(?someobj)).\n"
                    + "} GROUP BY ?s ?l ORDER BY DESC(?count) LIMIT 20", _searchTerm);
        }

        Query query = QueryFactory.create(q);
        QueryExecution qExe = QueryExecutionFactory.sparqlService(this.myURL, query);
        qExe.setTimeout(myTimeout);

        try {
            ResultSet results = qExe.execSelect();
            ArrayList<DisambiguityResult> temp = new ArrayList<>();

            while (results.hasNext()) {
                QuerySolution s = results.next();
                temp.add(new DisambiguityResult(s.get("s").toString(), s.get("l").toString(), s.get("count").asLiteral().getValue().toString()));
            }

            result = new DisambiguityResults(InterestingResult.Success, temp);
        } catch (QueryExceptionHTTP e) {
            result = new DisambiguityResults(InterestingResult.Timeout, null);
        } catch (Exception e) {
            result = new DisambiguityResults(InterestingResult.UnknownError, null);
        }
        return result;
    }

    /**
     * Executes the provided query and returns zero or more interesting paths
     *
     * @param _query Query to be executed
     * @return ArrayList of InterestingPaths
     */
    @Override
    public ArrayList<InterestingPath> FetchPaths(String _query) throws Exception {
        ArrayList<InterestingPath> result = new ArrayList<>();

        Query query = QueryFactory.create(_query);

        QueryExecution qExe = QueryExecutionFactory.sparqlService(this.myURL, query);
        qExe.setTimeout(myTimeout);
        ResultSet results = qExe.execSelect();

        List<String> headers = results.getResultVars();

        while (results.hasNext()) {
            InterestingPath path = new InterestingPath();
            QuerySolution r = results.next();
            for (String h : headers) {
                path.myPath.add(r.get(h).toString());
            }
            result.add(path);
        }

        // Clean up
        qExe.close();
        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public InterestingPath[] InterestingPaths(String[] _NSFilters, String _source, String _target, String _searchTerms, String _lang, int _minPath, int _maxPath, int _maxNumOfPaths, InterestingPathsThread _report) {

        List<InterestingPath> myPaths = new ArrayList<>();

        TDB tdb;
        if (TDBManager.containsModel(_source, myURL, _NSFilters)) {
            tdb = TDBManager.getModel(_source, myURL, _NSFilters);
            int size = tdb.getMyLength();

            if (size < _maxPath) {
                _report.ReportProgress("TDBStore contains partial model. Expanding model by " + (_maxPath - size) + " steps");
                TDBManager.ExpandTDB(_maxPath - size, tdb);
            } else {
                _report.ReportProgress("TDBStore contains model of sufficient size and settings. Using this model...");
            }
        } else {
            _report.ReportProgress("TDBStore contains no suitable model. Building new model.");
            tdb = TDBManager.CreateTDB(_source, myURL, _maxPath, _NSFilters);
        }
        tdb.Connect();
        ResultSet rs;
        int counter = 0;
        // Fetching paths
        for (int i = _minPath; i <= _maxPath; i++) {
            String[] queries = SPARQLFactory.GenerateQueries(i, _source, _target, _NSFilters);

            for (String q : queries) {
                ResultSet results = tdb.performSelect(q);
                List<String> headers = results.getResultVars();

                while (results.hasNext()) {
                    InterestingPath path = new InterestingPath();
                    QuerySolution r = results.next();
                    for (String h : headers) {
                        path.myPath.add(r.get(h).toString());
                    }
                    myPaths.add(path);
                    counter++;
                }
            }
            _report.ReportProgress("Status for k=" + i + " - " + counter + " paths found");
            counter = 0;
        }

        tdb.Close();
        // Fetching relevant literals
        String[] distinctNodes = GraphHelperUtil.DistinctURI(myPaths);
        _report.ReportProgress(distinctNodes.length + " distinct nodes found...");
        Multimap<String, String> literals = HashMultimap.create();
        String[] literalQueries = SPARQLFactory.GetMultipleLiterals(distinctNodes, _lang);

        for (String literalQuery : literalQueries) {
            SPARQL_ResultSet results = performSelect(literalQuery);

            if (results.validSet) {
                for (Vector<String> v1 : results.myData) {

                    if (v1.size() == 2) {
                        String sbj = v1.get(0);
                        String lit = v1.get(1);
                        literals.put(sbj, lit);
                    }
                }
            }
        }

        _report.ReportProgress(literals.size() + " literals fetched...");

        WordScorer myScorer = new WordScorer(_searchTerms, 1.0);
        InterestingPath[] paths = new InterestingPath[myPaths.size()];
        paths = myPaths.toArray(paths);

        // Scoring paths
        for (InterestingPath path : paths) {
            double score = 0.0;
            int intermediateNodes = 0;
            for (int j = 2; j < path.myPath.size() - 1; j = j + 2) {
                Collection<String> coll = literals.get(path.myPath.get(j));
                score += myScorer.ScoreDocuments(coll);
                intermediateNodes++;
            }
            path.myScore = (score) / ((double) intermediateNodes);
        }

        Arrays.sort(paths);
        if (_maxNumOfPaths <= paths.length) {
            return Arrays.copyOfRange(paths, 0, _maxNumOfPaths);
        } else {
            return Arrays.copyOfRange(paths, 0, paths.length);
        }
    }

    @Override
    public void setTimeout(int _timeout) {
        myTimeout = _timeout;
    }

    @Override
    public int getTimeout() {
        return myTimeout;
    }

    @Override
    public String getEndpointURL() {
        return myURL;
    }

    @Override
    public void setEndpointURL(String _newURI) {
        myURL = _newURI;
    }
}
