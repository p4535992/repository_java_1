/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package GUI.CustomControls.TabContainer;

import Graph.Interfaces.GraphInterface;
import com.mxgraph.view.mxGraph;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class GraphTab extends JPanel {

    private final TabMaster tabContainer;
    private String myName;
    private final JLabel myTitleText;
    private final JButton myCloseButton;
    private GraphTab mySelf;
    private final GraphInterface myGraph;
    private static final Icon close_icon = new ImageIcon(GraphTab.class.getResource("CloseIcon.png"));

    public GraphTab(TabMaster _tabContainer, String _name, GraphInterface _graph) {
        // Setting references
        this.tabContainer = _tabContainer;
        this.myName = _name;
        this.mySelf = this;
        this.myGraph = _graph;

        // Adding tab with empty title
        this.tabContainer.addTab(null, this);
        // Getting position of self
        int pos = this.tabContainer.indexOfComponent(this);

        // Constructing panel for title tab
        FlowLayout f = new FlowLayout(FlowLayout.CENTER, 10, 0);
        JPanel titlePanel = new JPanel(f);
        titlePanel.setOpaque(false);
        myTitleText = new JLabel(_name);

        // Constructing close button
        myCloseButton = new JButton();
        myCloseButton.setOpaque(false);
        myCloseButton.setBorder(null);
        myCloseButton.setFocusable(false);
        myCloseButton.setIcon(close_icon);

        ActionListener closeListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tabContainer.RemoveCustomTab(mySelf, myName);
            }
        };

        myCloseButton.addActionListener(closeListener);

        // Adding label and button and add border
        titlePanel.add(myTitleText);
        titlePanel.add(myCloseButton);
        titlePanel.setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));

        // insert title panel
        this.tabContainer.setTabComponentAt(pos, titlePanel);

        // Adding border and graph to tab
        this.setBorder(BorderFactory.createLineBorder(Color.black));
        this.setLayout(new GridLayout(1, 1));
        this.add(this.myGraph.GetGraphComponent());
        
        tabContainer.setSelectedIndex(pos);
    }
}
