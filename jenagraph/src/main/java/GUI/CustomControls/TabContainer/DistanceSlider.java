/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package GUI.CustomControls.TabContainer;

import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import javax.swing.JSlider;
import javax.swing.MenuElement;
import javax.swing.MenuSelectionManager;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

public class DistanceSlider extends JSlider implements MenuElement {

    private int myMin;
    private int myMax;
    private int oldValue;
    
    public DistanceSlider(int _min, int _max, int _init) {
        setBorder(new CompoundBorder(new TitledBorder("Min. Node Spacing"),
                new EmptyBorder(10, 10, 10, 10)));

        myMin = _min;
        myMax = _max;
        oldValue = _init;
        
        setMinimum(_min);
        setMaximum(_max);
        setValue(_init);
        
        setMajorTickSpacing(20);
        setMinorTickSpacing(10);
        setSnapToTicks(true);
    }

    @Override
    public void processMouseEvent(MouseEvent e, MenuElement path[],
            MenuSelectionManager manager) {
    }

    @Override
    public void processKeyEvent(KeyEvent e, MenuElement path[],
            MenuSelectionManager manager) {
    }

    @Override
    public void menuSelectionChanged(boolean isIncluded) {
    }

    @Override
    public MenuElement[] getSubElements() {
        return new MenuElement[0];
    }

    @Override
    public Component getComponent() {
        return this;
    }
    
    public boolean valueHasChanged() {
        if(oldValue == getValue()) {
            return false;
        }
        else {
            return true;
        }
    }
    
    public int Update() {
        oldValue = getValue();
        return oldValue;
    }
}
