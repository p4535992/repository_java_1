/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package GUI.CustomControls.TabContainer;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class InterestingTab {
    private final TabMaster tabContainer;
    private String myName;
    private JLabel myTitleText;
    private JButton myCloseButton;
    private InterestingTab mySelf;
    private InterestingPathsPanel myGraph;
    private static final Icon close_icon = new ImageIcon(GraphTab.class.getResource("CloseIcon.png"));

    public InterestingTab(TabMaster _tabContainer, String _name, InterestingPathsPanel _panel) {
        // Setting references
        this.tabContainer = _tabContainer;
        this.myName = _name;
        this.mySelf = this;
        this.myGraph = _panel;

        // Adding tab with empty title
        this.tabContainer.addTab(null, _panel);
        // Getting position of self
        int pos = this.tabContainer.indexOfComponent(_panel);

        // Constructing panel for title tab
        FlowLayout f = new FlowLayout(FlowLayout.CENTER, 10, 0);
        JPanel titlePanel = new JPanel(f);
        titlePanel.setOpaque(false);
        myTitleText = new JLabel(_name);

        // Constructing close button
        myCloseButton = new JButton();
        myCloseButton.setOpaque(false);
        myCloseButton.setBorder(null);
        myCloseButton.setFocusable(false);
        myCloseButton.setIcon(close_icon);

        ActionListener closeListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int pos = tabContainer.indexOfComponent(myGraph);
                tabContainer.RemoveCustomTab(pos);
            }
        };

        myCloseButton.addActionListener(closeListener);

        // Adding label and button and add border
        titlePanel.add(myTitleText);
        titlePanel.add(myCloseButton);
        titlePanel.setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));

        // insert title panel
        this.tabContainer.setTabComponentAt(pos, titlePanel);
        
        tabContainer.setSelectedIndex(pos);
    }
}
