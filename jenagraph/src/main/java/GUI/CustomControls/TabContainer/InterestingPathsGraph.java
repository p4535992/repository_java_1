/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package GUI.CustomControls.TabContainer;

import GUI.MainGUI;
import Graph.Implementations.GraphHelperUtil;
import Graph.Implementations.TripleStatement;
import Graph.InterestingPaths.InterestingPath;
import Graph.Interfaces.GraphInterface;
import com.mxgraph.layout.mxFastOrganicLayout;
import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGraphModel;
import com.mxgraph.model.mxICell;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.swing.util.mxMorphing;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxDomUtils;
import com.mxgraph.util.mxEvent;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.view.mxGraph;
import com.mxgraph.view.mxStylesheet;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.ArrayList;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class InterestingPathsGraph extends mxGraph implements GraphInterface, PopupMenuListener, ActionListener, MouseListener {

    private final mxGraphComponent myComponent;
    private mxFastOrganicLayout myLayout;
    private double myForceConstant = 100;
    private final String myModelName;
    private final InterestingPath[] myPaths;
    private Object startObj, endObj;
    private String startString, endString;
    private mxCell startCell, endCell;
    private final Document myDoc = mxDomUtils.createDocument();
    private final DistanceSlider mySlider = new DistanceSlider(30, 200, (int) myForceConstant);
    private JPopupMenu myBackgroundMenu = new JPopupMenu();

    // Initial coordinates for start and end vertex
    private int startX = 5, startY = 100, endX = 800, endY = 100;

    // Styles used in this graph
    private String edge = "edge", edgeSelect = "edgeSelect", edgeFade = "edgeFade";
    private String vertex = "vertex", vertexSelect = "vertexSelect", vertexFade = "vertexFade";

    public InterestingPathsGraph(String _model, InterestingPath[] _paths) {
        myModelName = _model;
        myPaths = _paths;

        // <editor-fold defaultstate="collapsed" desc="Graph Settings">
        this.myComponent = new mxGraphComponent(this);
        this.myComponent.setConnectable(false);
        this.setCellsEditable(false);
        this.setAllowDanglingEdges(false);
        this.setAllowLoops(false);
        this.setCellsDeletable(false);
        this.setCellsCloneable(false);
        this.setCellsDisconnectable(false);
        this.setDropEnabled(false);
        this.setSplitEnabled(false);
        this.setCellsBendable(false);
        this.setEdgeLabelsMovable(false);
        this.setVertexLabelsMovable(false);
        this.setCellsResizable(false);
        this.setAutoSizeCells(true);
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Configuring styles">
        mxStylesheet stylesheet = this.getStylesheet();

        // Edge styles
        stylesheet.putCellStyle(edge, GraphStyles.getEdgeBlunt());
        stylesheet.putCellStyle(edgeSelect, GraphStyles.getEdgeBluntSelected());
        stylesheet.putCellStyle(edgeFade, GraphStyles.getEdgeBluntFaded());

        // Vertex styles
        stylesheet.putCellStyle(vertex, GraphStyles.getInterestingVertex());
        stylesheet.putCellStyle(vertexSelect, GraphStyles.getVertexSelected());
        stylesheet.putCellStyle(vertexFade, GraphStyles.getVertexFaded());
        // </editor-fold>

        // <editor-fold defaultstate="collapsed" desc="Creating Graph">
        Object parent = this.getDefaultParent();
        this.getModel().beginUpdate();
        try {
            // Setting up initial nodes
            SetupEnds(parent);

            // Inserting paths
            for (int i = 0; i < myPaths.length; i++) {
                InsertPath(myPaths[i], parent, i);
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            this.getModel().endUpdate();
        }
        // </editor-fold>

        JMenuItem myItem = new JMenuItem("Clear Selection");
        myItem.addActionListener(this);
        myBackgroundMenu.add(myItem);

        myItem = new JMenuItem("Export to XML");
        myItem.addActionListener(this);
        myBackgroundMenu.add(myItem);

        // Adding min. distance slider
        myBackgroundMenu.addSeparator();
        myBackgroundMenu.add(mySlider);

        myBackgroundMenu.addPopupMenuListener(this);
        this.myComponent.getGraphControl().addMouseListener(this);
        this.myComponent.setToolTips(true);
    }

    public void RearrangeModel() {

        this.myLayout = new mxFastOrganicLayout(this);
        this.myLayout.setForceConstant(myForceConstant);
        Object parent = this.getDefaultParent();

        this.getModel().beginUpdate();
        try {
            this.myLayout.execute(parent);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            mxMorphing morph = new mxMorphing(myComponent, 20, 1.2, 20);
            morph.addListener(mxEvent.DONE, new mxIEventListener() {

                @Override
                public void invoke(Object arg0, mxEventObject arg1) {
                    getModel().endUpdate();
                }
            });
            morph.startAnimation();
        }
    }

    /**
     * Determines whether if a node with the specific URI is within the model
     *
     * @param _URI The URI to be searched for
     * @return Returns true iff model contains URI
     */
    private boolean ModelContainsCell(String _URI) {
        Object myCell = ((mxGraphModel) this.getModel()).getCell(_URI);
        if (myCell == null) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Determines whether v1 and v2 are connected either as v1 -> v2 or v2 ->
     * v1.
     *
     * @param _v1 Source node
     * @param _v2 Target node
     * @return Returns true iff the two nodes are connected.
     */
    private boolean CellsAreConnected(Object _v1, Object _v2) {
        mxCell v1 = (mxCell) _v1;
        mxCell v2 = (mxCell) _v2;

        for (int i = 0; i < v1.getEdgeCount(); i++) {
            mxICell edge = v1.getEdgeAt(i);
            mxICell target = edge.getTerminal(false);

            if (target.getId().equals(v2.getId())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public mxGraphComponent GetGraphComponent() {
        return this.myComponent;
    }

    /**
     * Iterates over a path and inserts it into the graph.
     *
     * @param _path Path to be inserted
     * @param _parent Parent object
     * @param _pathIndex Index of _path within myPaths
     */
    private void InsertPath(InterestingPath _path, Object _parent, int _pathIndex) {

        if (_path.myPath.size() == 5) {
            // Creating middle object
            Object v1 = InsertVertex(_path.myPath.get(2), _pathIndex, 2, _parent);
            // Connecting start to middle
            InsertEdge(_path.myPath.get(1), _pathIndex, 1, _parent, startObj, v1);

            // Connecting middle to end
            InsertEdge(_path.myPath.get(3), _pathIndex, 3, _parent, v1, endObj);
        } else {
            // Connecting start to middle
            Object oldObject = InsertVertex(_path.myPath.get(2), _pathIndex, 2, _parent);
            InsertEdge(_path.myPath.get(1), _pathIndex, 1, _parent, startObj, oldObject);

            for (int i = 2; i < _path.myPath.size() - 3; i = i + 2) {
                Object newObject = InsertVertex(_path.myPath.get(i + 2), _pathIndex, (i + 2), _parent);
                InsertEdge(_path.myPath.get(i + 1), _pathIndex, (i + 1), _parent, oldObject, newObject);
                oldObject = newObject;
            }

            // Connecting middle to end
            InsertEdge(_path.myPath.get(_path.myPath.size() - 2), _pathIndex, (_path.myPath.size() - 2), _parent, oldObject, endObj);
        }
    }

    private void InsertPathInverse(InterestingPath _path) {

    }

    /**
     * Sets up start and end nodes with fixed location.
     *
     * @param _parent
     */
    private void SetupEnds(Object _parent) {
        startString = myPaths[0].myPath.get(0);
        endString = myPaths[0].myPath.get(myPaths[0].myPath.size() - 1);

        // Inserting start and changing label to auto-size node
        startObj = insertVertex(_parent, startString, "", startX, startY, 80, 30, vertex);
        myComponent.labelChanged(startObj, GraphHelperUtil.cropURI(startString), null);
        startCell = (mxCell) ((mxGraphModel) this.getModel()).getCell(startString);

        // Inserting end and changing label to auto-size node
        endObj = insertVertex(_parent, endString, "", endX, endY, 80, 30, vertex);
        myComponent.labelChanged(endObj, GraphHelperUtil.cropURI(endString), null);
        endCell = (mxCell) ((mxGraphModel) this.getModel()).getCell(endString);

        // Setting start and end as unmoveable
        this.setCellStyles(mxConstants.STYLE_MOVABLE, "false", new Object[]{startObj, endObj});
    }

    /**
     * returns longname as a tooltip
     *
     * @param cell to return tool tip for
     * @return Returns long form URI/Blank Node ID/Literal
     */
    @Override
    public String getToolTipForCell(Object cell) {
        if (cell instanceof mxCell) {
            mxCell c = (mxCell) cell;
            try {
                Object value = c.getValue();
                Element elt = (Element) value;
                return elt.getAttribute("longname");
            } catch (Exception e) {
                return "Cell Element not found";
            }
        } else {
            return super.getToolTipForCell(cell);
        }
    }

    /**
     * Creates and inserts a vertex into graph.
     *
     * @param _URI URI for vertex
     * @param _pathIdx Index within myPaths
     * @param _unitIdx Index within myPaths[_unitIdx].myPath
     * @param _parent Parent object
     * @return Returns object created
     */
    private Object InsertVertex(
            String _URI, Integer _pathIdx, Integer _unitIdx,
            Object _parent) {

        Element data = myDoc.createElement("data");
        data.setAttribute("longname", _URI);
        data.setAttribute("shortname", GraphHelperUtil.cropURI(_URI));
        data.setAttribute("pathidx", _pathIdx.toString());
        data.setAttribute("unitidx", _unitIdx.toString());

        Object vertex = insertVertex(_parent, null, data, 350, 200, 80, 30, this.vertex);
        updateCellSize(vertex);
        return vertex;
    }

    /**
     * Creates an edge between two nodes. The value for the edge is a document
     * containing shortname, longname, index within myPaths and index within
     * myPaths[_pathidx].myPath
     *
     * @param _URI URI for vertex
     * @param _pathIdx Index within myPaths
     * @param _unitIdx Index within myPaths[_unitIdx].myPath
     * @param _parent Parent object
     * @param _from Edge going from this object
     * @param _to Edge going to this object
     * @return Returns edge created as object
     */
    private Object InsertEdge(
            String _URI, Integer _pathIdx, Integer _unitIdx,
            Object _parent, Object _from, Object _to) {
        Element data = myDoc.createElement("data");
        data.setAttribute("longname", _URI);
        data.setAttribute("shortname", GraphHelperUtil.cropURI(_URI));
        data.setAttribute("pathidx", _pathIdx.toString());
        data.setAttribute("unitidx", _unitIdx.toString());
        Object newEdge = insertEdge(_parent, null, data, _from, _to, edge);
        return newEdge;
    }

    @Override
    public String convertValueToString(Object cell) {
        if (cell instanceof mxCell) {
            mxCell myCell = (mxCell) cell;
            if (myCell.isVertex()) {
                Object value = myCell.getValue();
                if (value instanceof Element) {
                    Element elt = (Element) value;
                    if (elt.getTagName().equalsIgnoreCase("data")) {
                        return elt.getAttribute("shortname");
                    }
                }
            } else if (myCell.isEdge()) {
                return "";
            }

        }
        return super.convertValueToString(cell);
    }

    @Override
    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
    }

    @Override
    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
        if (mySlider.valueHasChanged()) {
            int newValue = mySlider.Update();

            // Updating force
            myForceConstant = (double) newValue;

            // Refreshing visualisation
            RearrangeModel();
        }
    }

    @Override
    public void popupMenuCanceled(PopupMenuEvent e) {
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JMenuItem item = (JMenuItem) e.getSource();
        switch (item.getText()) {
            case "Clear Selection":
                ClearPathSelection();
                break;
            case "Export to XML":
                Export2XML();
                break;
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (SwingUtilities.isRightMouseButton(e)) {
            JScrollPane pane = ((JScrollPane) myComponent);
            int vertOffset = pane.getVerticalScrollBar().getValue();
            int horOffset = pane.getHorizontalScrollBar().getValue();

            Object[] myCells = this.getSelectionCells();
            if (myCells.length == 0) {
                this.myBackgroundMenu.show(
                        this.myComponent, e.getX() - horOffset, e.getY() - vertOffset);
            } else if (myCells.length == 1) {
            } else if (myCells.length > 1) {
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    public void setPathSelected(int _idx) {
        if (_idx > -1 && _idx < myPaths.length) {
            getPathObjects(_idx);
        }
    }

    private void getPathObjects(int _idx) {
        Object[] pathObjects = new Object[myPaths[_idx].myPath.size()];

        Object[] edges = getChildEdges(getDefaultParent());
        Object[] pathEdges = new Object[myPaths[_idx].Steps()];
        int ptr = 0;

        for (Object e : edges) {
            if (memberOfPath(e, _idx)) {
                pathEdges[ptr] = e;
                ptr++;
            }
        }

        Object[] vertex = getChildVertices(getDefaultParent());
        Object[] pathVertices = new Object[myPaths[_idx].IntermediateNodes()];
        ptr = 0;

        for (Object v : vertex) {
            if (memberOfPath(v, _idx) && !(startObj.equals(v) || endObj.equals(v))) {
                pathVertices[ptr] = v;
                ptr++;
            }
        }
        Object parent = this.getDefaultParent();
        this.getModel().beginUpdate();
        try {
            setCellStyle(edgeFade, edges);
            setCellStyle(edgeSelect, pathEdges);

            setCellStyle(vertexFade, vertex);
            setCellStyle(vertexSelect, pathVertices);
            setCellStyle(this.vertex, new Object[]{startObj, endObj});
            setCellStyles(mxConstants.STYLE_MOVABLE, "false", new Object[]{startObj, endObj});
        } finally {
            this.getModel().endUpdate();
            refresh();
        }
    }

    /**
     * Clears any path selection and returns all cells to their default
     * visualisation.
     */
    public void ClearPathSelection() {
        Object[] edges = getChildEdges(getDefaultParent());
        Object[] vertices = getChildVertices(getDefaultParent());
        Object parent = this.getDefaultParent();

        this.getModel().beginUpdate();
        try {
            setCellStyle(edge, edges);
            setCellStyle(edge, vertices);
            setCellStyles(mxConstants.STYLE_MOVABLE, "false", new Object[]{startObj, endObj});
        } finally {
            this.getModel().endUpdate();
            refresh();
        }
    }

    /**
     * Determines whether _cell is part of myPaths[_idx]
     *
     * @param _cell Cell to be verified
     * @param _idx Target path
     * @return True iff _cell is a member of myPaths[_idx]
     */
    private boolean memberOfPath(Object _cell, Integer _idx) {
        boolean result = false;
        if (_cell instanceof mxCell) {
            Object value = ((mxCell) _cell).getValue();
            if (value instanceof Element) {
                if (((Element) value).getAttribute("pathidx") == null ? _idx.toString() == null : ((Element) value).getAttribute("pathidx").equals(_idx.toString())) {
                    result = true;
                }
            }
        }
        return result;
    }

    private void Export2XML() {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("Paths");
            doc.appendChild(rootElement);

            for (int i = 0; i < myPaths.length; i++) {
                Element path = doc.createElement("Path");
                rootElement.appendChild(path);

                for (String s : myPaths[i].myPath) {
                    Element step = doc.createElement("Step");
                    step.appendChild(doc.createTextNode(s));
                    path.appendChild(step);
                }
                Element score = doc.createElement("Score");
                score.appendChild(doc.createTextNode(((Double) myPaths[i].myScore).toString()));
                path.appendChild(score);
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            transformer.setOutputProperty(OutputKeys.VERSION, "1.0");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            DOMSource source = new DOMSource(doc);
            File output = new File(myPaths.length + ".xml");
            StreamResult result = new StreamResult(output);

            transformer.transform(source, result);
            MainGUI.WriteToLog("Paths exported to: " + output.getAbsolutePath());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void Clear() {
        this.setCellsDeletable(true);
        this.getModel().beginUpdate();
        try {
            ((mxGraphModel) this.getModel()).clear();
        } catch (Exception e) {
            System.out.println("Error!");
        } finally {
            this.getModel().endUpdate();
            this.refresh();
            this.setCellsDeletable(false);
        }
    }
}
