/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package GUI.CustomControls.SPARQLBox;

import GUI.Threads.SPARQLConstructThread;
import GUI.Threads.SPARQLSelectThread;
import GUI.Threads.SPARQLAskThread;
import GUI.MainGUI;
import Graph.Implementations.SPARQL_ResultSet;
import javax.swing.JTextArea;

public class SPARQLBox extends JTextArea {
    
    public SPARQLBox() {
    }
    
    /**
     * Attempts to execute the current content as a SPARQL expression
     */
    public void RunExpression() {
        
        if(this.getText().length() > 0 && MainGUI.GetSPARQLTarget() != null)
        {
            String modelTarget = (String)MainGUI.GetSPARQLTarget();
            
            switch(MainGUI.myData.getQueryType(this.getText())) {
                case SELECT:
                    SPARQLSelectThread mySelectThread = new SPARQLSelectThread(this.getText(), modelTarget);
                    mySelectThread.execute();
                    break;
                case CONSTRUCT:
                    SPARQLConstructThread myConstructThread = new SPARQLConstructThread(this.getText(), modelTarget);
                    myConstructThread.execute();
                    break;
                case DESCRIBE:
                    
                    break;
                case ASK:
                    SPARQLAskThread myAskThread = new SPARQLAskThread(this.getText(), modelTarget);
                    myAskThread.execute();
                    break;
                case UNKNOWN:
                    MainGUI.WriteToLog("An unknown error has occurred during SPARQL execution.");
                    break;
                case ERROR:
                    MainGUI.WriteToLog("There is an error in your SPARQL expression");
                    break;
            }
        }
    }
}
