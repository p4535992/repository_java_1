/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package GUI.Threads;

import GUI.MainGUI;
import Graph.Enumerator.NodeType;
import Graph.Implementations.TripleStatement;
import java.util.ArrayList;
import javax.swing.SwingWorker;

public class DescribeThread extends SwingWorker<Integer, Object> {
    private final String myCenter;
    private final String myModel;
    private final NodeType myCenterType;
    
    public DescribeThread(String _center, String _model, NodeType _type) {
        myCenter = _center;
        myModel = _model;
        myCenterType = _type;
    }
    
    @Override
    protected Integer doInBackground() throws Exception {
        ArrayList<TripleStatement> result = new ArrayList<>();
        if(myCenterType == NodeType.Vertex) {
            result = MainGUI.myData.CBD(myModel, myCenter);
            MainGUI.WriteToLog("Finding Concise Bounded Description...");
        }
        else if(myCenterType == NodeType.Literal) {
            result = MainGUI.myData.SCBD(myModel, myCenter);
            MainGUI.WriteToLog("Finding Symmetric Concise Bounded Description...");
        }
        MainGUI.VisualiseGraph(result, myModel);
        return result.size();
    }
    
    @Override
    protected void done() {
        try {
            int count = (int)get();
            MainGUI.WriteToLog(count + " triples fetched.");
        } catch (Exception e) {
        }
    }
}
