/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package GUI.Threads;

import GUI.MainGUI;
import Graph.Enumerator.NodeType;
import Graph.Enumerator.VisualiseStatus;
import Graph.Implementations.VisualiseWrapper;
import javax.swing.SwingWorker;

/**
  * Queries a model for for triples around an explicite
  * or implicit center. 
  */
public class VisualiseModelThread extends SwingWorker<VisualiseWrapper, VisualiseWrapper> {

    private final String modelName;
    private String newCenter = null;
    private NodeType newCenterType = null;

    public VisualiseModelThread(String _modelName) {
        this.modelName = _modelName;
    }

    public VisualiseModelThread(String _modelName, String _newCenter, NodeType _newCenterType) {
        this.modelName = _modelName;
        this.newCenter = _newCenter;
        this.newCenterType = _newCenterType;
    }

    @Override
    protected VisualiseWrapper doInBackground() throws Exception {
        VisualiseWrapper result = null;
        switch (MainGUI.myData.isReadyToVisualise(modelName)) {
            case Ready:
                result = ExecuteVisualise();
                break;
            case NeedsFocalPoint:
                MainGUI.WriteToLog("Please set focal vertex for model");
                result = new VisualiseWrapper(null, VisualiseStatus.NeedFocalPoint);
                break;
            case ModelNotFound:
                MainGUI.WriteToLog("An error occurred doing model retrieval");
                result = new VisualiseWrapper(null, VisualiseStatus.UnableToFindModel);
                break;
        }
        return result;
    }

    @Override
    protected void done() {
        try {
            VisualiseWrapper result = get();
            switch (result.myStatus) {
                case Timeout:
                    if(result.myTriples.size() > 0) {
                        MainGUI.WriteToLog("Query timed out during execution. Partial results shown(" + result.myTriples.size() + ")");
                        MainGUI.VisualiseGraph(result.myTriples, modelName);
                    }
                    else {
                        MainGUI.WriteToLog("Query timed out during execution.");
                    }
                    break;
                case Success:
                    MainGUI.WriteToLog(result.myTriples.size() + " triples fetched.");
                    MainGUI.VisualiseGraph(result.myTriples, modelName);
                    break;
                case UnknownError:
                    MainGUI.WriteToLog("An unknown error occurred during query execution");
                    break;
            }
        } catch (Exception e) {
            MainGUI.WriteToLog("An unknown error occurred during query execution");
        }
    }

    private VisualiseWrapper ExecuteVisualise() {
        VisualiseWrapper result;
        if (this.newCenter == null) {
            MainGUI.WriteToLog("Fetching neighbours of "
                    + MainGUI.myData.getCenterVertex(modelName)
                    + "..."
            );
            result = MainGUI.myData.CenterOn(modelName);
        } else {
            MainGUI.WriteToLog("Fetching neighbours of "
                    + newCenter + "...");
            result = MainGUI.myData.CenterOn(modelName, newCenter, newCenterType);
        }
        return result;
    }
}
