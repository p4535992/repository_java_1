/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package GUI.Threads;

import GUI.MainGUI;
import Graph.InterestingPaths.InterestingPath;
import java.util.List;
import javax.swing.SwingWorker;

public class InterestingPathsThread extends SwingWorker<InterestingPath[], String> {
    
    private final String myModel;
    private final String[] myNSFilters;
    private final String mySource, myTarget;
    private final String mySearchTerms, mylanguage;
    private final int myMaxPath, myMinPath, myMaxNumOfPaths;
    
    public InterestingPathsThread(
            String _model,
            String[] _NSfilters,
            String _source, String _target,
            String _searchTerm,
            String _lang,
            int _minPath, int _maxPath, int _maxNumOfPaths) {
        
        myModel = _model;
        myNSFilters = _NSfilters;
        mySource = _source;
        myTarget = _target;
        mySearchTerms = _searchTerm;
        mylanguage = _lang;
        myMinPath = _minPath;
        myMaxPath = _maxPath;
        myMaxNumOfPaths = _maxNumOfPaths;
    }
    
    @Override
    protected InterestingPath[] doInBackground() throws Exception {
        InterestingPath[] result = MainGUI.myData.InterestingPaths(
                myModel,
                myNSFilters,
                mySource,
                myTarget,
                mySearchTerms,
                mylanguage,
                myMinPath,
                myMaxPath,
                myMaxNumOfPaths,
                this
        );
        return result;
    }
    
    @Override
    protected void done() {
        try {
            InterestingPath[] result = get();
            MainGUI.VisualiseInterestingPaths(myModel, result);
        } catch (Exception e) {
            MainGUI.WriteToLog("Unknown error during Interesting path search.");
            MainGUI.WriteToLog("Error: " + e.getMessage());
        }
    }
    
    @Override
    protected void process(List<String> chunks) {
        for(String s : chunks) {
            MainGUI.WriteToLog(s);
        }
    }
    
    public void ReportProgress(String _msg) {
        this.publish(_msg);
    }
}
