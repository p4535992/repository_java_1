/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package GUI.Threads;

import Graph.Implementations.SPARQL_Thread_Result;
import Graph.InterestingPaths.InterestingPath;
import Graph.Interfaces.DataSourceInterface;
import org.apache.jena.sparql.engine.http.QueryExceptionHTTP;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;

public class PathFetcherThread extends Thread {
    private final DataSourceInterface myMaster;
    private final int myResultIndex;
    private final String myQuery;
    private final ConcurrentLinkedQueue<InterestingPath> myResultList;
    private SPARQL_Thread_Result[] myReports;
    
    public PathFetcherThread(DataSourceInterface _master, int _resultIndex, String _query, ConcurrentLinkedQueue<InterestingPath> _resultList, SPARQL_Thread_Result[] _reports) {
        myMaster = _master;
        myResultIndex = _resultIndex;
        myQuery = _query;
        myResultList = _resultList;
        myReports = _reports;
    }
    
    @Override
    public void run() {
        try {
            ArrayList<InterestingPath> paths = myMaster.FetchPaths(myQuery);
            for(InterestingPath ip : paths) {
                if(!myResultList.contains(ip)) {
                    myResultList.add(ip);
                }
            }
            //myResultList.addAll(paths);
            myReports[myResultIndex] = SPARQL_Thread_Result.Success;
        } catch(QueryExceptionHTTP e) {
            myReports[myResultIndex] = SPARQL_Thread_Result.Timeout;
        } catch (Exception e) {
            myReports[myResultIndex] = SPARQL_Thread_Result.UnknownError;
        }
    }
}
