/**
Copyright 2014 Dines Klewing Juul Madsen(thedrows@gmail.com), Heidi Olivia Munksgaard(heidi.olivi@gmail.com)

This file is part of GraphHelper.
GraphHelper is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
GraphHelper is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with GraphHelper. If not, see http://www.gnu.org/licenses/.
*/

package GUI.Threads;

import GUI.MainGUI;
import Graph.Enumerator.ExportModel;
import Graph.Enumerator.FileFormat;
import javax.swing.SwingWorker;

/**
 *
 * @author Elipson
 */
public class ExportModelThread extends SwingWorker<ExportModel, Object> {

    private String model;
    private String path;
    private FileFormat format;
    
    public ExportModelThread(String _model, String _path, FileFormat _format)
    {
        this.model = _model;
        this.path = _path;
        this.format = _format;
    }
    @Override
    protected ExportModel doInBackground() throws Exception {
        ExportModel result = MainGUI.myData.ExportModel(model, path, format);
        return result;
    }
    @Override
    protected void done() {
        try {
            ExportModel result = get();
            
            switch(result) {
                case Success:
                    MainGUI.WriteToLog(path + " loaded succesfully.");
                    break;
                case FileParseError:
                    MainGUI.WriteToLog("unable to export " + path);
                    break;
                case ModelNotFound:
                    MainGUI.WriteToLog("Unable to finde model" + model);
                    break;
            }
        } catch (Exception e) {
            MainGUI.WriteToLog(
                    "An unknown error occurred while exporting " + this.path);
        }
    }
}
